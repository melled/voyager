<?php

namespace Flares\Debug;

/**
 * Class Dump
 * @package Flares\Debug
 */
class Dump
{
    public function variable($var)
    {
        echo '<pre>';
        var_dump($var);
        echo '</pre>';
    }
}