<?php

namespace Flares\Debug;

/**
 * Class ErrorHandler
 * @package Flares\Debug
 */
class ErrorHandler
{
    public function register()
    {
        set_error_handler([$this, 'handleError']);
        set_exception_handler([$this, 'handleException']);
    }

    public function restoreDefaultHandler()
    {
        restore_error_handler();
        restore_exception_handler();
    }

    public function handleError($severity, $message, $file, $line)
    {
        throw new \ErrorException($message, 0, $severity, $file, $line);
    }

    public function handleException($exception)
    {
        ob_end_clean();

        $this->printTitle('Error!');

        $this->printTitle('Error type',2);
        $this->printr(get_class($exception));

        $this->printTitle('Message',2);
        $this->printr($exception->getMessage());

        $this->printTitle('In file', 2);
        $this->printr($exception->getFile() . ':' . $exception->getLine());

        $this->printTitle('Code', 2);
        $codeLine = file($exception->getFile())[$exception->getLine() - 1];
        $this->printr($codeLine);

        $this->printTitle('Stack trace', 2);
        $this->printr($exception->getTraceAsString());
    }

    protected function printr($text)
    {
        echo "<pre>$text</pre>";
    }

    protected function printTitle($text, $level = 1)
    {
        echo "<h$level>$text</h$level>";
    }
}