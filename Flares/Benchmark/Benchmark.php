<?php

namespace Flares\Benchmark;

class Benchmark
{
    private $marks = [];

    public function __construct()
    {
        $this->mark('_init');
    }

    /**
     * @param string $name
     */
    public function mark($name)
    {
        $this->marks[$name] = [
            'time' => microtime(true),
            'memory' => memory_get_usage()
        ];
    }

    /**
     * @param string $markName
     * @return int
     * @throws \Exception
     */
    public function getMemoryUsage($markName = null)
    {
        if ($markName === null) {
            return memory_get_usage();
        }

        if ($this->marks[$markName]) {
            throw new \Exception('Mark not found');
        }

        return $this->marks[$markName]['memory'];
    }

    /**
     * @param string $firstMarkName
     * @param string $secondMarkName
     * @param int $precision
     * @return float
     * @throws \Exception
     */
    public function elapsedTime($firstMarkName, $secondMarkName, $precision = 5)
    {
        if (!isset($this->marks[$firstMarkName]['time']) ||
            !isset($this->marks[$secondMarkName]['time'])) {
            throw new \Exception('Mark not found');
        }
        $time = abs($this->marks[$firstMarkName]['time'] - $this->marks[$secondMarkName]['time']);
        return round($time, $precision);
    }
}