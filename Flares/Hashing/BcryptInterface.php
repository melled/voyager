<?php

namespace Flares\Hashing;

/**
 * Interface BcryptInterface
 * @package Flares\Hashing
 */
interface BcryptInterface
{
    /**
     * @param string $value
     * @param int $cost
     * @return string
     */
    public function hash($value, $cost = 10);

    /**
     * @param string $value
     * @param string $hash
     * @return bool
     */
    public function verify($value, $hash);

    /**
     * @param string $hash
     * @param int $cost
     * @return bool
     */
    public function needsRehash($hash, $cost = 10);
}