<?php

namespace Flares\Hashing;

/**
 * Class Bcrypt
 * @package Flares\Hashing
 */
class Bcrypt implements BcryptInterface
{

    /**
     * @param string $value
     * @param int $cost
     * @return string
     */
    public function hash($value, $cost = 10)
    {
        return password_hash($value, PASSWORD_BCRYPT, ['cost' => $cost]);
    }

    /**
     * @param string $value
     * @param string $hash
     * @return bool
     */
    public function verify($value, $hash)
    {
        return password_verify($value, $hash);
    }

    /**
     * @param string $hash
     * @param int $cost
     * @return bool
     */
    public function needsRehash($hash, $cost = 10)
    {
        return password_needs_rehash($hash, PASSWORD_BCRYPT, ['cost' => $cost]);
    }
}