<?php

namespace Flares\Filtering\Handler;

class Trim
{
    public function handle($value)
    {
        return trim($value);
    }
}