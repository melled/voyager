<?php

namespace Flares\Filtering;

use Flares\Validation\Exception;

class Filter implements FilterInterface
{
    protected $handlersClassMap = [
        'trim' => '\Flares\Filtering\Handler\Trim'
    ];

    protected $callableHandlers = [];

    public function handle($value, $filters)
    {
        if (is_string($filters)) {
            return $this->singleFiltering($value, $filters);
        } else if (is_array($filters)) {
            return $this->multipleFiltering($value, $filters);
        } else if (is_callable($filters)) {
            return call_user_func($filters, $value);
        }
        throw new Exception('Bad filter');
    }

    protected function singleFiltering($value, $filter)
    {
        if (isset($this->handlersClassMap[$filter])) {
            $handler = new $this->handlersClassMap[$filter];
            return $handler->handle($value);
        } else if (isset($this->callableHandlers[$filter])) {
            return call_user_func($this->callableHandlers[$filter], $value);
        }
        throw new Exception('Filter not found');
    }

    protected function multipleFiltering($value, array $filters)
    {
        foreach ($filters as $filter) {
            $value = $this->singleFiltering($value, $filter);
        }
        return $value;
    }

    public function addHandler($handlerName, callable $handler)
    {
        $this->callableHandlers[$handlerName] = $handler;
    }
}