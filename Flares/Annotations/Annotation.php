<?php

namespace Flares\Annotations;

/**
 * Class Annotation
 * @package Flares\Annotations
 */
class Annotation
{
    private $name;

    private $arguments;

    /**
     * @param string $name
     * @param array $args
     */
    public function __construct($name, $args)
    {
        $this->name = $name;
        $this->arguments = $args;
    }

    /**
     * @return int
     */
    public function getParamsCount()
    {
        return count($this->arguments);
    }

    /**
     * @return array
     */
    public function getArguments()
    {
        return $this->arguments;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}