<?php

namespace Flares\Annotations;

/**
 * Interface ParserInterface
 * @package Flares\Annotations
 */
interface ParserInterface
{
    public function parseClass($class);

    public function parseMethod($class, $methodName);

    public function parseProperty($class, $propertyName);
}