<?php

namespace Flares\Annotations;

/**
 * Class Parser
 * @package Flares\Annotations
 */
class Parser implements ParserInterface
{
    /**
     * @param string|object $class
     * @return Annotations
     */
    public function parseClass($class)
    {
        $class = new \ReflectionClass($class);
        $phpDoc = $class->getDocComment();
        return $this->parseAnnotations($phpDoc);
    }

    /**
     * @param string|object $class
     * @param string $methodName
     * @return Annotations
     */
    public function parseMethod($class, $methodName)
    {
        $method = new \ReflectionMethod($class, $methodName);
        $phpDoc = $method->getDocComment();
        return $this->parseAnnotations($phpDoc);
    }

    /**
     * @param string|object $class
     * @param string $propertyName
     * @return Annotations
     */
    public function parseProperty($class, $propertyName)
    {
        $property = new \ReflectionProperty($class, $propertyName);
        $phpDoc = $property->getDocComment();
        return $this->parseAnnotations($phpDoc);
    }

    /**
     * @param string $doc
     * @return Annotations
     */
    protected function parseAnnotations($doc)
    {
        $annotations = new Annotations();
        $docExplode = explode('*', $doc);

        foreach ($docExplode as $line) {
            $annotation = $this->parseAnnotation($line);
            if ($annotation) {
                $annotations->add($annotation);
            }
        }
        return $annotations;
    }

    private function parseAnnotation($line)
    {
        if ($this->isAnnotation($line)) {
            return new Annotation(
                $this->parseName($line),
                $this->parseArgs($line)
            );
        }
        return null;
    }

    private function parseName($line)
    {
        $start = strrpos($line, '@') + 1;
        $end = (strpos($line, '{') ? strpos($line, '{') - 1 : strlen($line)) - 1;
        return trim(substr($line, $start, $end));
    }

    private function parseArgs($l)
    {
        $pos = [strpos($l, '{'), strrpos($l, '}')];
        $params = substr($l, $pos[0], $pos[1]);
        return json_decode($params, true);
    }

    /**
     * @todo Абстрагироваться от ctype
     * @param string $line
     * @return boolean
     */
    private function isAnnotation($line)
    {
        $line = trim($line);
        return (@$line[0] === '@') && ctype_upper(@$line[1]);
    }
}