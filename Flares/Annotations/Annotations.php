<?php

namespace Flares\Annotations;

/**
 * Class Annotations
 * @package Flares\Annotations
 */
class Annotations implements \Iterator
{
    private $annotations;

    private $position = 0;

    /**
     * @param array $annotations
     */
    public function __construct(array $annotations = [])
    {
        $this->annotations = $annotations;
    }

    /**
     * @param Annotation $annotation
     */
    public function add($annotation)
    {
        $this->annotations[] = $annotation;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function has($name)
    {
        foreach ($this->annotations as $annotation) {
            if ($annotation->getName() === $name) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $name
     * @return null | Annotation
     */
    public function get($name)
    {
        if ($this->has($name)) {
            foreach ($this->annotations as $annotation) {
                if ($annotation->getName() === $name) {
                    return $annotation;
                }
            }
        }
        return null;
    }

    public function current()
    {
        return $this->annotations[$this->position];
    }

    public function next()
    {
        $this->position++;
    }

    public function rewind()
    {
        $this->position = 0;
    }

    public function key()
    {
        return $this->position;
    }

    public function valid()
    {
        return array_key_exists($this->position, $this->annotations);
    }
}