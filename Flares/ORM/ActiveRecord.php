<?php

namespace Flares\ORM;

use Flares\Database\SelectBuilder;
use Flares\DI\Container;

/**
 * Class ActiveRecord
 * @package Flares\ORM
 * @Table {"name": "users"}
 * @Relations {}
 */
class ActiveRecord
{
    /**
     * Новый, несохраненный в базе
     */
    const STATUS_NEW = 0;
    /**
     * Сохраненный в базе, поля соответствуют полям из базы
     */
    const STATUS_SAVED = 1;
    /**
     * Сохраненный в базе, но поля объекта были изменены
     */
    const STATUS_CHANGED = 2;
    /**
     * Запись в базе была удалена, но объект остался
     */
    const STATUS_DELETED = 3;

    /**
     * Текущий статус объекта
     */
    protected $recordStatus = self::STATUS_NEW;

    /**
     * @var \Flares\DI\ContainerInterface
     */
    protected $di;


    static public function find($id)
    {
        $di = Container::getStaticDI();
        $driver = $di->make('db_driver');
        $select = $di->make('db_selectBuilder');
        $metaData = $di->make('orm_metaData');

        $tableName = $metaData->getTableName(new static());
        $primaryKeyName = $metaData->getPrimaryKeyName(new static());

        if (is_null($primaryKeyName)) {
            throw new Exception('Primary key not defined');
        }

        $select->columns('*')->from($tableName)->where("{$primaryKeyName} = ?");
        $result = $driver->select($select, [$id]);
        if (!$result) {
            throw new Exception('Model not found');
        }
        vardump($result);
    }

    public function findOne($id)
    {

    }

    public function findBy($column, $value)
    {

    }

    public function findAll()
    {

    }

    public function fillModel($data = null)
    {

    }

    static public function createAndFill($data = [])
    {

    }

    protected function hasOne($model, $foreignKey = null, $localColumn = null)
    {

    }

    protected function belongsTo($model, $localColumn = null, $foreignKey = null)
    {

    }

    protected function hasMany($model, $foreignKey = null, $localColumn = null)
    {

    }

    protected function manyToMany($model, $pivot, $firstKey = null, $secondKey = null)
    {

    }

    protected function hasManyThrough($firstModel, $secondModel, $firstKey = null, $secondKey = null)
    {

    }

    protected function getRelatedModels()
    {
        $relationsData = $this->getRelationsData();
        $models = [];
        foreach ($relationsData as $key => $data) {
            if (is_array($data[0])) {

            }
        }
        self::createAndFill($relationsData);
    }

    protected function getModelFromDb($select)
    {

    }

    protected function loadRelations($model)
    {
        foreach ($this->relations as $relation) {
            if ($relation->isMultiple()) {

            } else {

            }
        }
    }v
}