<?php

namespace Flares\ORM\Relations;

abstract class Relation
{
    /**
     * @var bool
     */
    protected $multiple;

    protected $foreignKey;

    protected $table;

    public function getForeignKey()
    {
        return $this->foreignKey;
    }

    public function getTableName()
    {
        return $this->table;
    }
}