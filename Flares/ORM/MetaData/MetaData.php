<?php

namespace Flares\ORM\MetaData;

use Flares\DI\ContainerInterface;
use Flares\ORM\ActiveRecord;
use Flares\ORM\Exception;

class MetaData implements MetaDataInterface
{
    /**
     * @var ActiveRecord
     */
    protected $model;

    /**
     * @var ContainerInterface
     */
    protected $di;

    /**
     * @param ContainerInterface $di
     * @param ActiveRecord $model
     */
    public function __construct(ContainerInterface $di, ActiveRecord $model)
    {
        $this->di = $di;
        $this->model = $model;
    }

    /**
     * @return string
     */
    public function getTableName()
    {
        $class = new \ReflectionClass($this->model);
        try {
            return $this->getTableNameFromProperty($class);
        } catch (Exception $error) {
            return $this->getTableNameFromModelName($class);
        }
    }

    /**
     * @return array
     */
    public function getColumnNames()
    {
        try {
            $property = new \ReflectionProperty($this->model, 'columns');
            return $property->getValue($this->model);
        } catch (\ReflectionException $error) {
            return $this->getColumnNamesFromDb();
        }
    }

    public function getColumns()
    {
        try {
            $property = new \ReflectionProperty($this->model, 'columns');
            return $property;
        } catch (\ReflectionException $error) {
            return $this->describeColumns();
        }
    }

    /**
     * @return bool
     */
    public function isAutoIncrement()
    {
        return (bool)$this->getAsset('autoIncrement', true);
    }

    /**
     * @return string|null
     */
    public function getPrimaryKeyName()
    {
        return $this->getAsset('primaryKey', 'id');
    }

    /**
     * @throws Exception
     * @return \Flares\ORM\Relations\Relation []
     */
    public function getRelations()
    {
        $relations = $this->getAsset('relations');
        if (!$relations) {
            throw new Exception('Relations not found');
        }
        return $relations;
    }

    private function getTableNameFromModelName(\ReflectionClass $model)
    {
        $modelName = $model->getName();
        return $this->underScope($modelName);
    }

    private function getTableNameFromProperty(\ReflectionClass $model)
    {
        $properties = $model->getDefaultProperties();
        $className = @$properties['tableName'];
        if ($className !== null) {
            return $className;
        }
        throw new Exception("Property 'tableName' not found");
    }

    private function underScope($text)
    {
        $chars = str_split($text);
        for ($i = 0; $i < count($chars); $i++) {
            if (ctype_upper($chars[$i])) {
                $chars[$i] = ($i ? '_' : '') . mb_strtolower($chars[$i]);
            }
        }
        return implode('', $chars);
    }

    private function getAsset($propertyName, $default = false)
    {
        try {
            $property = new \ReflectionProperty($this->model, $propertyName);
            return $property->getValue($this->model);
        } catch (\ReflectionException $error) {
            return $default;
        }
    }

    private function describeColumns()
    {
        $tableName = $this->getTableName();
        $driver = $this->di->make('db_driver');
        return $driver->describeColumns($tableName);
    }

    private function getColumnNamesFromDb()
    {
        $columns = $this->describeColumns();
        $names = [];
        foreach ($columns as $column) {
            $names[] = $column->getName();
        }
        return $names;
    }
}