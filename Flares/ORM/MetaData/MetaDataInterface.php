<?php

namespace Flares\ORM\MetaData;

interface MetaDataInterface
{
    /**
     * @return mixed
     */
    public function getTableName();
}