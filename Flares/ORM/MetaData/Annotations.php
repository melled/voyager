<?php

namespace Flares\ORM\MetaData;

use Flares\DI\Container;
use Flares\DI\ContainerInterface;
use Flares\DI\InjectionAwareInterface;

class Annotations implements MetaDataInterface, InjectionAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $di;

    /**
     * @param ContainerInterface $di
     */
    public function setDI($di)
    {
        $this->di = $di;
    }

    /**
     * @return ContainerInterface
     */
    public function getDI()
    {
        return $this->di;
    }

    /**
     * @param object|string $model
     * @return string
     */
    public function getTableName($model)
    {
        $parser = $this->di->make('annotations_parser');
        $annotations = $parser->parseClass($model);
        foreach ($annotations as $annotation) {
            if ($annotation->getName() == 'Table') {
                $arguments = $annotation->getArguments();
                if (isset($arguments['name'])) {
                    return $arguments['name'];
                }
            }
        }
    }

    public function getColumns($model)
    {
        $parser = $this->di->make('annotations_parser');
        $columns = [];
        $class = new \ReflectionClass($model);
        $properties = $class->getProperties();
        foreach ($properties as $property) {
            $annotations = $parser->parseProperty($model, $property->getName());
            foreach ($annotations as $annotation) {
                if ($annotation->getName() == 'Column') {
                    $columns[$property->getName()] = $annotation;
                }
            }
        }
        return $columns;
    }

    public function getPrimaryKeyName($model)
    {
        $columns = $this->getColumns($model);
        foreach ($columns as $name => $column) {
            $arguments = $column->getArguments();
            if (isset($arguments['primaryKey']) && $arguments['primaryKey']) {
                return [
                    'propertyName' => $name,
                    'columnName' => @$arguments['name']
                ];
            }
        }
    }

    public function getColumn($model, $columnName)
    {
        $columns = $this->getColumns($model);
        foreach ($columns as $name => $column) {
            $arguments = $column->getArguments();
            if (isset($arguments['primaryKey']) && $arguments['primaryKey']) {
                return [$name, ];
            }
        }
    }

    public function getHasOneRelations($model)
    {
        $parser = $this->di->make('annotations_parser');
        $relations = [];
        $class = new \ReflectionClass($model);
        $properties = $class->getProperties();
        foreach ($properties as $property) {
            $annotations = $parser->parseProperty($model, $property->getName());
            foreach ($annotations as $annotation) {
                if ($annotation->getName() == 'HasOne') {
                    $columns[$property->getName()] = $annotation;
                }
            }
        }
        return $relations;
    }

}