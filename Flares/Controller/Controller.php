<?php

namespace Flares\Controller;

use Flares\DI\ContainerInterface;
use Flares\DI\InjectionAwareInterface;

class Controller implements ControllerInterface, InjectionAwareInterface
{
    /**
     * @var ContainerInterface
     */
    protected $di;

    /**
     * @param ContainerInterface $di
     */
    public function setDI(ContainerInterface $di)
    {
        $this->di = $di;
    }

    /**
     * @return ContainerInterface
     */
    public function getDI()
    {
        return $this->di;
    }

    /**
     * @param string $serviceName
     * @return mixed
     * @throws Exception
     */
    protected function getService($serviceName)
    {
        if ($this->di->hasService($serviceName)) {
            return $this->di->make($serviceName);
        }

        throw new Exception('Service not found');
    }
}