<?php

namespace Flares\Validation;

abstract class AbstractValidationRule implements ValidationRuleInterface
{
    protected $definition;

    public function __construct(array $definition)
    {
        $this->definition = $definition;
    }
}