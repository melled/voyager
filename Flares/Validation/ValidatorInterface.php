<?php

namespace Flares\Validation;

interface ValidatorInterface
{
    /**
     * @param string $fieldName
     */
    public function __construct($fieldName);

    /**
     * @return string
     */
    public function getFieldName();

    /**
     * @param ValidationRuleInterface $rule
     */
    public function addRule(ValidationRuleInterface $rule);

    /**
     * @param mixed $value
     * @return ValidationResultInterface
     */
    public function validate($value);
}