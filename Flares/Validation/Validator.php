<?php

namespace Flares\Validation;

class Validator implements ValidatorInterface
{
    protected $rules = [];

    protected $fieldName;

    public function __construct($fieldName)
    {
        $this->fieldName = $fieldName;
    }

    public function getFieldName()
    {
        return $this->fieldName;
    }

    public function addRule(ValidationRuleInterface $rule)
    {
        $this->rules[] = $rule;
    }

    public function validate($value)
    {
        $errorMessages = [];
        foreach ($this->rules as $rule) {
            try {
                $rule->validate($this, $value);
            } catch (Exception $error) {
                $errorMessages[] = $error->getMessage();
            }
        }
        return new ValidationResult($this->fieldName, $errorMessages);
    }
}