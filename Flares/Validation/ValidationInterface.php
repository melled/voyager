<?php

namespace Flares\Validation;

interface ValidationInterface
{
    /**
     * @param string $fieldName
     * @param ValidationRuleInterface[] $validationRules
     */
    public function add($fieldName, array $validationRules = []);

    /**
     * @param $value
     * @return ValidationResultInterface[]
     */
    public function validate($value);
}