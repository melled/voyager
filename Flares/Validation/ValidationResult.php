<?php

namespace Flares\Validation;

class ValidationResult implements ValidationResultInterface
{
    /**
     * @var string
     */
    protected $fieldName;

    /**
     * @var array
     */
    protected $messages;

    protected $result = true;

    protected $type;

    /**
     * @param string $fieldName
     * @param array $messages
     */
    public function __construct($fieldName, $type, array $messages = [])
    {
        $this->fieldName = $fieldName;
        $this->messages = $messages;
        $this->type = $type;
        if ($messages) {
            $this->result = false;
        }
    }

    /**
     * @return string
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * @return array
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * @return bool
     */
    public function getResult()
    {
        return $this->result;
    }

    public function getType()
    {
        return $this->type;
    }
}