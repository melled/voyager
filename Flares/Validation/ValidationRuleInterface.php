<?php

namespace Flares\Validation;

interface ValidationRuleInterface
{
    public function __construct(array $definition);

    public function validate(ValidatorInterface $validator, $value);
}