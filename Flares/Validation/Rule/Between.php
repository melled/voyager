<?php

namespace Flares\Validation\Rule;

use Flares\Validation\AbstractValidationRule;
use Flares\Validation\Exception;
use Flares\Validation\ValidatorInterface;

class Between extends AbstractValidationRule
{
    protected $defaultMessage = 'Поле ":field" должно быть в диапазоне от :min до :max';

    public function validate(ValidatorInterface $validator, $value)
    {
        if ($value <= $this->definition['min'] || $value >= $this->definition['max']) {
            $message = $this->defaultMessage;
            if (@$this->definition['errorMessage'] !== null) {
                $message = $this->definition['errorMessage'];
            }
            $message = str_replace(
                [':max', ':min', ':field'],
                [
                    $this->definition['max'],
                    $this->definition['min'],
                    $validator->getFieldName()
                ],
                $message
            );
            throw new Exception($message);
        }
        return true;
    }
}