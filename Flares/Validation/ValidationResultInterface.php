<?php

namespace Flares\Validation;

interface ValidationResultInterface
{
    /**
     * @param string $fieldName
     * @param array $messages
     */
    public function __construct($fieldName, $type, array $messages = []);

    /**
     * @return string
     */
    public function getFieldName();

    /**
     * @return array
     */
    public function getMessages();

    /**
     * @return bool
     */
    public function getResult();
}