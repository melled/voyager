<?php

namespace Flares\Validation;

class Validation
{
    protected $fields;

    /**
     * @param string $fieldName
     * @param ValidationRuleInterface[] $validationRules
     */
    public function add($fieldName, array $validationRules = [])
    {
        $this->fields[$fieldName] = $validationRules;
    }

    /**
     * @param $value
     * @return ValidationResultInterface[]
     */
    public function validate($value)
    {
        $errorMessages = [];
        foreach ($this->fields as $fieldName => $rules) {
            $validator = new Validator($fieldName);
            foreach ($rules as $rule) {
                $validator->addRule($rule);
            }
            $result = $validator->validate($value);
            if (!$result->getResult()) {
                $errorMessages[] = $result;
            }
        }
        return $errorMessages;
    }
}