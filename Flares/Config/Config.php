<?php

namespace Flares\Config;

/**
 * Class Config
 * @package Flares\Config
 */
class Config implements \ArrayAccess
{
    protected $config;

    public function __construct(array $config = [])
    {
        $this->config = $config;
    }

    public function offsetUnset($offset)
    {
        unset($this->config[$offset]);
    }

    public function offsetGet($offset)
    {
        if (isset($this->config[$offset])) {
            return $this->config[$offset];
        }

        return null;
    }

    public function offsetExists($offset)
    {
        return isset($this->config[$offset]);
    }

    public function offsetSet($offset, $value)
    {
        if ($offset === null) {
            $this->config[] = $value;
        } else {
            $this->config[$offset] = $value;
        }
    }
}