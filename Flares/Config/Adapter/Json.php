<?php

namespace Flares\Config\Adapter;

use Flares\Config\Config;

/**
 * Class Php
 * @package Flares\Config\Adapter
 */
class Json extends Config
{
    /**
     * @param string $file
     */
    public function __construct($file)
    {
        parent::__construct(json_decode(file_get_contents($file), true));
    }
}