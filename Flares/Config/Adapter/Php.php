<?php

namespace Flares\Config\Adapter;

use Flares\Config\Config;

/**
 * Class Php
 * @package Flares\Config\Adapter
 */
class Php extends Config
{
    /**
     * @param string $file
     */
    public function __construct($file)
    {
        parent::__construct(require $file);
    }
}