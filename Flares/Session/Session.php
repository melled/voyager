<?php

namespace Flares\Session;

/**
 * Class Session
 * @package Flares\Session
 */
class Session implements SessionInterface
{
    protected $started = false;

    /**
     * @return bool
     */
    public function start()
    {
        if (headers_sent()) {
            return false;
        }

        if ($this->started) {
            return false;
        }

        if ($this->getStatus() === self::SESSION_ACTIVE) {
            return false;
        }

        $this->started = true;
        return session_start();
    }

    /**
     * @param bool $removeData
     * @return bool
     */
    public function destroy($removeData = false)
    {
        if ($removeData) {
            $_SESSION = [];
        }

        return session_destroy();
    }

    /**
     * @return bool
     */
    public function isStarted()
    {
        return $this->started;
    }

    /**
     * @param string $key
     * @param string $value
     */
    public function set($key, $value)
    {
        $_SESSION[$key] = $value;
    }

    /**
     * @param string $key
     * @param mixed $defaultValue
     * @return mixed
     */
    public function get($key, $defaultValue = null)
    {
        if (isset($_SESSION[$key])) {
            return $_SESSION[$key];
        }

        return $defaultValue;
    }

    /**
     * @param string $key
     * @return bool
     */
    public function has($key)
    {
        return isset($_SESSION[$key]);
    }

    /**
     * @param string $key
     */
    public function remove($key)
    {
        unset($_SESSION[$key]);
    }

    /**
     * @param string $id
     */
    public function setId($id)
    {
        session_id($id);
    }

    /**
     * @return string
     */
    public function getId()
    {
        return session_id();
    }

    /**
     * @param bool $deleteOldSession
     */
    public function regenerateId($deleteOldSession = false)
    {
        session_regenerate_id($deleteOldSession);
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        if (function_exists('session_status')) {
            return session_status();
        }

        return self::SESSION_NONE;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        session_name($name);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return session_name();
    }
}