<?php

namespace Flares\Session;

/**
 * Interface SessionInterface
 * @package Flares\Session
 */
interface SessionInterface
{
    const SESSION_DISABLED = 0;

    const SESSION_NONE = 1;

    const SESSION_ACTIVE = 2;

    /**
     * @return bool
     */
    public function start();

    /**
     * @param bool $removeData
     * @return bool
     */
    public function destroy($removeData = false);

    /**
     * @return bool
     */
    public function isStarted();

    /**
     * @param string $key
     * @param string $value
     */
    public function set($key, $value);

    /**
     * @param string $key
     * @param mixed $defaultValue
     * @return mixed
     */
    public function get($key, $defaultValue = null);

    /**
     * @param string $key
     * @return bool
     */
    public function has($key);

    /**
     * @param string $key
     */
    public function remove($key);

    /**
     * @param string $id
     */
    public function setId($id);

    /**
     * @return string
     */
    public function getId();

    /**
     * @param bool $deleteOldSession
     */
    public function regenerateId($deleteOldSession = false);

    /**
     * @return int
     */
    public function getStatus();

    /**
     * @param string $name
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getName();
}