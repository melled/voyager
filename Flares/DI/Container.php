<?php

namespace Flares\DI;

class Container implements ContainerInterface
{
    private $services = [];

    static private $staticDI;

    public function set($serviceName, $action, $shared = false)
    {
        $this->services[$serviceName] = new Service($serviceName, $action, $shared);
    }

    public function setShared($serviceName, $action)
    {
        $this->set($serviceName, $action, true);
    }

    /**
     * @param string $serviceName
     * @param $parameters
     * @return object
     */
    public function make($serviceName, $parameters = null)
    {
        $instance = null;
        if ($this->hasService($serviceName)) {
            $instance = $this->getService($serviceName)->make($parameters);
        }
        $this->injectDI($instance);

        return $instance;
    }

    /**
     * @param string $serviceName
     * @return Service
     */
    public function getService($serviceName)
    {
        if ($this->hasService($serviceName)) {
            return $this->services[$serviceName];
        }
        return null;
    }

    /**
     * @param string $nameService
     * @return bool
     */
    public function hasService($nameService)
    {
        if (isset($this->services[$nameService])) {
            return true;
        }
        return false;
    }

    /**
     * @return array
     */
    public function getServices()
    {
        return $this->services;
    }

    /**
     * @param string $serviceName
     */
    public function removeService($serviceName)
    {
        if ($this->hasService($serviceName)) {
            unset($this->services[$serviceName]);
        }
    }

    /**
     * @param ContainerInterface $di
     */
    static public function setStaticDI(ContainerInterface $di)
    {
        self::$staticDI = $di;
    }

    /**
     * @return ContainerInterface
     */
    static public function getStaticDI()
    {
        return self::$staticDI;
    }

    private function injectDI($instance)
    {
        if ($instance instanceof InjectionAwareInterface) {
            $instance->setDI($this);
        }
    }
}