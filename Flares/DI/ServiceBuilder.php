<?php

namespace Flares\DI;

class ServiceBuilder
{
    /**
     * @param ContainerInterface $di
     * @param array $action
     * @param array $parameters
     * @return Object
     * @throws \Exception
     */
    public function build($di, $action, $parameters = null)
    {
        if (!isset($action['className'])) {
            throw new \Exception('Not passed class name');
        }

        $class = $this->getReflectionClass($action['className']);
        $instance = null;

        if ($this->constructorInjection($di, $action, $class)) {
            $instance = $this->constructorInjection($di, $action, $class);
        } else {
            $instance = $this->createInstance($action['className'], $parameters);
        }

        $this->methodInjection($di, $action, $instance);
        $this->propertyInjection($di, $action, $instance);

        return $instance;
    }

    private function constructorInjection($di, $action, $reflectionClass)
    {
        if (isset($action['arguments'])) {
            $parameters = $this->extractParameters($di, $action['arguments']);
            if ($parameters) {
                return $reflectionClass->newInstanceArgs($parameters);
            }
            return $reflectionClass->newInstance();
        }
        return null;
    }

    private function methodInjection($di, $action, $instance)
    {
        if (isset($action['methods'])) {
            foreach ($action['methods'] as $method) {
                $func = new ReflectionMethod($instance, $method['name']);
                $args = $func->getParameters();
                $parameters = [];
                if (isset($method['arguments'])) {
                    $parameters = $this->extractParameters($di, $method['arguments']);
                }
                if (!$this->checkParameters($args, $parameters)) {
                    throw new \Exception('Bad parameters');
                }
                if ($parameters) {
                    $func->invokeArgs($instance, $parameters);
                } else {
                    $func->invoke($instance);
                }
            }
        }
    }

    private function propertyInjection($di, $action, $instance)
    {
        if (isset($action['properties'])) {
            foreach ($action['properties'] as $property) {
                $this->setProperty($property, $di, $instance);
            }
        }
    }

    private function setProperty($property, $di, $instance)
    {
        if (!isset($property['name'])) {
            throw new \Exception('Undefined property name');
        }

        if (!isset($property['value']['type'])) {
            throw new \Exception('Undefined property type');
        }

        $type = $property['value']['type'];
        $prop = new ReflectionProperty($instance, $property['name']);

        switch ($type) {
            case 'service':
                $prop->setValue($instance, $di->make($property['value']['name']));
                break;
            case 'parameter':
                $prop->setValue($instance, $property['value']['value']);
                break;
            default:
                throw new \Exception('Undefined type property');
        }

    }

    private function createInstance($className, $parameters = null)
    {
        $reflectionClass = $this->getReflectionClass($className);
        $args = $reflectionClass->getConstructor()->getParameters();

        if (!$this->checkParameters($args, $parameters)) {
            throw new \Exception('Bad parameters');
        }

        if ($parameters) {
            return $reflectionClass->newInstanceArgs($parameters);
        }
        return $reflectionClass->newInstance();
    }

    private function checkParameters($arguments, $parameters)
    {
        if (count($arguments) > count($parameters)) {
            if (!$arguments[count($parameters)]->isOptional()) {
                return false;
            }
        }
        return true;
    }

    private function getReflectionClass($className)
    {
        if (!class_exists($className)) {
            throw new \Exception("Class $this->action does not exist");
        }

        $class = new \ReflectionClass($className);

        if (!$class->isInstantiable()) {
            throw new \Exception("Access to non-public constructor of class $this->action");
        }

        return $class;
    }

    private function extractParameters($di, $arguments)
    {
        $parameters = [];
        foreach ($arguments as $argument) {
            if (!isset($argument['type'])) {
                throw new \Exception('Undefined type argument');
            }

            if ($argument['type'] == 'service') {
                if (!$di->hasService($argument['name'])) {
                    throw new \Exception('Service not found');
                }
                $parameters[] = $di->make($argument['name']);
            } elseif ($argument['type'] == 'parameter') {
                if (!isset($argument['value'])) {
                    throw new \Exception('Value not found');
                }
                $parameters[] = $argument['value'];
            } else {
                throw new \Exception('Undefined type argument');
            }
        }
        return $parameters;
    }
}