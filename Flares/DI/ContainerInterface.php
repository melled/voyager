<?php

namespace Flares\DI;

interface ContainerInterface
{
    /**
     * @param string $serviceName
     * @param mixed $action
     * @param bool $shared
     */
    public function set($serviceName, $action, $shared);

    /**
     * @param string $serviceName
     * @param mixed $action
     */
    public function setShared($serviceName, $action);

    /**
     * @param string $serviceName
     * @param array $parameters
     * @return mixed
     */
    public function make($serviceName, $parameters = null);

    /**
     * @param string $serviceName
     * @return ServiceInterface
     */
    public function getService($serviceName);

    /**
     * @param string $nameService
     * @return bool
     */
    public function hasService($nameService);

    /**
     * @return ServiceInterface[]
     */
    public function getServices();

    /**
     * @param string $serviceName
     */
    public function removeService($serviceName);
}