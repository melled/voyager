<?php

namespace Flares\DI;

interface InjectionAwareInterface
{
    /**
     * @param ContainerInterface $di
     */
    public function setDI(ContainerInterface $di);

    /**
     * @return ContainerInterface
     */
    public function getDI();
}