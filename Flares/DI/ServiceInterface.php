<?php

namespace Flares\DI;

interface ServiceInterface
{
    /**
     * @param string $name
     * @param mixed $action
     * @param bool $alwaysNewInstance
     */
    public function __construct($name, $action, $alwaysNewInstance = true);

    /**
     * @return string
     */
    public function getName();

    /**
     * @return mixed
     */
    public function getAction();

    /**
     * @return bool
     */
    public function isAlwaysNewInstance();

    /**
     * @param string $name
     */
    public function setName($name);

    /**
     * @param mixed $action
     */
    public function setAction($action);

    /**
     * @param bool $status
     */
    public function setAlwaysNewInstance($status);

    /**
     * @param mixed $params
     * @param ContainerInterface $di
     * @return mixed
     */
    public function make($params = null, $di = null);
}