<?php

namespace Flares\DI;

class Service implements ServiceInterface
{
    private $name;

    private $action;

    private $alwaysNewInstance;

    private $savedInstance;

    public function __construct($name, $action, $alwaysNewInstance = true)
    {
        $this->name = $name;
        $this->action = $action;
        $this->alwaysNewInstance = $alwaysNewInstance;
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAction()
    {
        return $this->action;
    }

    public function isAlwaysNewInstance()
    {
        return $this->alwaysNewInstance;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function setAction($action)
    {
        $this->action = $action;
    }

    public function setAlwaysNewInstance($status)
    {
        $this->alwaysNewInstance = $status;
    }

    public function make($parameters = null, $di = null)
    {
        $instance = $this->getSavedInstance();

        if (!is_null($instance)) {
            return $instance;
        }

        $instance = $this->createInstance($this->action, $parameters);

        $this->saveInstance($instance);

        return $instance;
    }

    private function getSavedInstance()
    {
        if (!$this->alwaysNewInstance) {
            if (!is_null($this->savedInstance)) {
                return $this->savedInstance;
            }
        }
        return null;
    }

    private function saveInstance($instance)
    {
        if($this->alwaysNewInstance && is_null($this->savedInstance)) {
            $this->alwaysNewInstance = true;
            $this->savedInstance = $instance;
        }
    }

    private function createInstanceByClassName($parameters = null)
    {
        if (!class_exists($this->action)) {
            throw new \Exception("Class $this->action does not exist");
        }

        $class = new \ReflectionClass($this->action);

        if (!$class->isInstantiable()) {
            throw new \Exception("Access to non-public constructor of class $this->action");
        }

        $instance = null;

        if (is_null($parameters)) {
            $instance = $class->newInstance();
        } else {
            $instance = $class->newInstanceArgs($parameters);
        }

        return $instance;
    }

    private function createInstanceByClosure($parameters = null)
    {
        if (is_null($parameters)) {
            return call_user_func($this->action);
        } else {
            return call_user_func_array($this->action, $parameters);
        }
    }

    private function createInstanceByArray($action, $parameters = null)
    {
        $builder = new ServiceBuilder();
        return $builder->build($this, $action, $parameters);
    }

    private function createInstance($action, $parameters)
    {
        if (is_string($this->action)) {
            return $this->createInstanceByClassName($parameters);

        } elseif ($action instanceof \Closure) {
            return $this->createInstanceByClosure($parameters);

        } elseif(is_array($this->action)) {
            return $this->createInstanceByArray($action, $parameters);

        } elseif (is_object($this->action)) {
            return $this->action;

        } else {
            throw new \Exception('Unknown action');

        }
    }
}