<?php

namespace Flares\Routing;

class Pattern
{
    private $path;

    private $paramTypes;

    private $pattern;

    private $types = [
        'num' => '/^\+?\d+$/',
        'string' => '/.*/'
    ];

    public function __construct($path, $paramTypes = [])
    {
        $this->path = $path;
        $this->paramTypes = $paramTypes;

        $this->transform();
        $this->defineTypes();
    }

    public function getElements()
    {
        return $this->pattern;
    }

    public function match($path)
    {
        $exploded = $this->explodePath($path);

        if (!$this->matchLength($exploded)) {
            return false;
        }

        if (!$this->matchPath($exploded)) {
            return false;
        }

        if (!$this->matchOutPath($exploded)) {
            return false;
        }

        return true;
    }

    public function addPrefix($prefix)
    {
        $this->path = $prefix . '/' . $this->path;
        $this->pattern = [];
        $this->transform();
    }

    private function matchOutPath($path) {
        for ($i = count($path); $i < count($this->pattern); $i++) {
            $element = $this->pattern[$i];
            if ($element->isRequired()) {
                return false;
            }
        }
        return true;
    }

    private function matchPath($path)
    {
        for ($i = 0; $i < count($path); $i++) {
            $element = $this->pattern[$i];
            if (!$element->match($path[$i])) {
                return false;
            }
        }
        return true;
    }

    private function matchLength($path)
    {
        if (count($path) <= count($this->pattern)){
            return true;
        }
    }

    private function defineTypes()
    {
        if (!$this->pattern) {
            return;
        }
        foreach ($this->pattern as $element) {
            if ($element->isParam()) {
                if ($this->hasElementInParamTypes($element)) {
                    $this->setMatchElement($element);
                }
            }
        }
    }

    private function setMatchElement($element)
    {
        $regex = $this->paramTypes[$element->getName()];
        if (isset($this->types[$regex])) {
            $element->setMatch($this->types[$regex]);
        } else {
            $element->setMatch($this->paramTypes[$element->getName()]);
        }
    }

    private function hasElementInParamTypes($element)
    {
        if (isset($this->paramTypes[$element->getName()])) {
            return true;
        }
        return false;
    }

    private function transform()
    {
        $exploded = $this->explodePath($this->path);
        foreach ($exploded as $element) {
            $this->pattern[] = new PatternElement($element);
        }
    }

    static public function explodePath($path)
    {
        return array_values(
            array_filter(explode('/', $path), function($a) {
                return $a != '';
            })
        );
    }
}