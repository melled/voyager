<?php

namespace Flares\Routing;

/**
 * Class Router
 * @package Flares\Routing
 */
class Router
{
    const ACTION_DELIMITER = ':';

    const NOT_FOUND = 404;

    const SUCCESS = 200;

    const NOT_HANDLED = 102;

    private $routes = [];

    private $handledCode = self::NOT_HANDLED;

    private $action = null;

    private $notFoundAction = null;

    private $params = [];

    /**
     * @param string $pattern
     * @param string $action
     * @param array $paramTypes
     * @param string $name
     */
    public function addRoute($pattern, $action, array $paramTypes = [], $name = null)
    {
        $this->routes[] = new Route($pattern, $action, $paramTypes, $name);
    }

    /**
     * @param string $prefix
     * @param array $routes
     */
    public function addGroup($prefix, array $routes)
    {
        foreach ($routes as $route) {
            $route->addPrefix($prefix);
            $this->addRoute($route);
        }
    }

    /**
     * @param string $path
     */
    public function handle($path)
    {
        foreach ($this->routes as $route) {
            if ($route->match($path)) {
                $this->handledCode = self::SUCCESS;
                $this->action = $this->parseAction($route->getAction());
                $this->params = $route->getParams($path);
                return;
            }
        }
        $this->handledCode = self::NOT_FOUND;
    }

    /**
     * @param string $action
     */
    public function notFound($action)
    {
        $this->notFoundAction = $this->parseAction($action);
    }

    public function getAction()
    {
        if ($this->handledCode == self::SUCCESS) {
            return $this->action;
        }
        return $this->notFoundAction;
    }

    public function getParams()
    {
        return $this->params;
    }

    private function parseAction($action)
    {
        $delimiter = strpos($action, self::ACTION_DELIMITER);

        return [
            'controller' => substr($action, 0, $delimiter),
            'action' => substr($action, $delimiter + 1)
        ];
    }
}