<?php

namespace Flares\Routing;

class Route
{
    private $action;

    private $name;

    private $pattern;

    public function __construct($pattern, $action, array $paramTypes = [], $name = null)
    {
        $this->action = $action;
        $this->pattern = new Pattern($pattern, $paramTypes);
        $this->name = $name;
    }

    public function addPrefix($prefix)
    {
        $this->pattern->addPrefix($prefix);
    }

    public function getAction()
    {
        return $this->action;
    }

    public function getPattern()
    {
        return $this->pattern;
    }

    public function getName()
    {
        return $this->name;
    }

    public function match($path)
    {
        return $this->pattern->match($path);
    }

    public function getParams($path)
    {
        $exploded = Pattern::explodePath($path);
        $params = [];
        if ($this->match($path)) {
            for ($i = 0; $i < count($exploded); $i++) {
                if ($this->pattern->getElements()[$i]->isParam()) {
                    $params[$this->pattern->getElements()[$i]->getName()] = $exploded[$i];
                }
            }
        }
        return $params;
    }
}