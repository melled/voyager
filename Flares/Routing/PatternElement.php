<?php

namespace Flares\Routing;

class PatternElement
{
    const STATUS_STATIC = 0;
    const STATUS_PARAM = 1;

    const MARKER_PARAM = '@';
    const MARKER_REQUIRED = '?';

    private $element;

    private $name;

    private $type = [
        'status' => self::STATUS_STATIC,
        'required' => true,
        'match' => '/.*/'
    ];

    public function __construct($element)
    {
        $this->element = $element;
        $this->setType();
        $this->extractName();
    }

    public function isParam()
    {
        if ($this->type['status'] == self::STATUS_PARAM) {
            return true;
        }
        return false;
    }

    public function isRequired()
    {
        return $this->type['required'];
    }

    public function setMatch($match)
    {
        $this->type['match'] = $match;
    }

    public function getName()
    {
        return $this->name;
    }

    public function match($pathElement)
    {
        if ($this->matchStatic($pathElement) || $this->matchParam($pathElement)) {
            return true;
        }
        return false;
    }

    private function matchStatic($pathElement)
    {
        if ($this->type['status'] == self::STATUS_STATIC) {
            if ($pathElement === $this->name) {
                return true;
            }
        }
    }

    private function matchParam($pathElement)
    {
        if ($this->type['status'] == self::STATUS_PARAM) {
            if (preg_match($this->type['match'], $pathElement)) {
                return true;
            }
        }
    }

    private function extractName()
    {
        $length = strlen($this->element);
        $offset = 0;

        if ($this->isParam()) {
            $offset = strlen(self::MARKER_PARAM);
        }
        if (!$this->isRequired()) {
            $length = strlen($this->element) - strlen(self::MARKER_REQUIRED) - 1;

        }
        $this->name = substr($this->element, $offset, $length);
    }

    /**
     * @todo несколько символьные маркеры
     */
    private function setType()
    {
        $first = substr($this->element, 0, 1);
        $last = substr($this->element, strlen($this->element) - 1, 1);

        if ($first === self::MARKER_PARAM) {
            $this->type['status'] = self::STATUS_PARAM;
        }
        if ($last === self::MARKER_REQUIRED) {
            $this->type['required'] = false;
        }
    }
}