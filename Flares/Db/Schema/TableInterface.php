<?php

namespace Flares\Db\Schema;

/**
 * Interface TableInterface
 * @package Flares\Db\Schema
 * @author Mell <mellevmikhail@gmail.com>
 * @version 1.0.0
 * @copyright 2015 Flares Framework
 */
interface TableInterface
{

    /**
     * @param string $name
     */
    public function __construct($name);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * @param array|string $type
     * @param string $columnName
     * @return ColumnInterface
     */
    public function addColumn($type, $columnName);

    /**
     * @return ColumnInterface[]
     */
    public function getColumns();

    /**
     * @param string $columnName
     * @return ColumnInterface
     */
    public function getColumn($columnName);

    /**
     * @param $columnName
     * @return ColumnInterface
     */
    public function addAutoIncrementColumn($columnName);

    /**
     * @param string $name
     * @param array|string $columns
     * @param string $referenceTable
     * @param array|string $referenceColumns
     * @param string $onDelete
     * @return $this
     */
    public function addForeignKey($name, $columns, $referenceTable, $referenceColumns, $onDelete);

    /**
     * @param string $name
     * @param array|string $columns
     * @return $this
     */
    public function addIndex($name, $columns);

    /**
     * @param array|string $columns
     * @return $this
     */
    public function addPrimaryKey($columns);

    /**
     * @param string $name
     * @param array|string $columns
     * @return $this
     */
    public function addUniqueKey($name, $columns);

}