<?php

namespace Flares\Db\Schema;

/**
 * Interface ColumnInterface
 * @package Flares\Db\Schema
 * @author Mell <mellevmikhail@gmail.com>
 * @version 1.0.0
 * @copyright 2015 Flares Framework
 */
interface ColumnInterface
{

    /**
     * @param string $name
     * @param string $type
     * @param int $size
     */
    public function __construct($name, $type, $size = null);

    /**
     * @param string $name
     * @return $this
     */
    public function setName($name);

    /**
     * @return string
     */
    public function getName();

    /**
     * @param string $type
     * @return $this
     */
    public function setType($type);

    /**
     * @param int $size
     * @return $this
     */
    public function setSize($size);

    /**
     * @return int
     */
    public function getSize();

    /**
     * @param bool $increment
     * @return $this
     */
    public function setAutoIncrement($increment = true);

    /**
     * @param bool $notNull
     * @return $this
     */
    public function setNotNull($notNull = true);

    /**
     * @param bool $primaryKey
     * @return $this
     */
    public function setPrimaryKey($primaryKey = true);

    /**
     * @param mixed $defaultValue
     * @return $this
     */
    public function setDefaultValue($defaultValue);

    /**
     * @param bool $unsigned
     * @return $this
     */
    public function setUnsigned($unsigned = true);

    /**
     * @param string $columnName
     * @return $this
     */
    public function setPositionAfter($columnName);

    /**
     * @param bool $first
     * @return $this
     */
    public function setPositionFirst($first = true);

    /**
     * @return bool
     */
    public function isAutoIncrement();

    /**
     * @return bool
     */
    public function isNumber();

    /**
     * @return mixed
     */
    public function isNotNull();

    /**
     * @return bool
     */
    public function isPrimaryKey();

    /**
     * @return bool
     */
    public function isUnsigned();

    /**
     * @return string
     */
    public function getType();

    /**
     * @return mixed
     */
    public function getDefaultValue();

    /**
     * @return bool
     */
    public function isFirst();

    /**
     * @return string
     */
    public function getPositionAfter();

}