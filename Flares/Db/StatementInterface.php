<?php

namespace Flares\Db;

/**
 * Interface StatementInterface
 * @package Flares\Db
 * @author Mell <mellevmikhail@gmail.com>
 * @version 1.0.0
 * @copyright 2015 Flares Framework
 */
interface StatementInterface
{

    /**
     * @return int
     */
    public function getColumnCount();

    /**
     * @return int
     */
    public function getRowCount();

    /**
     * @param $mode
     * @return mixed
     */
    public function fetch($mode = null);

    /**
     * @param $mode
     * @return mixed
     */
    public function fetchAll($mode = null);

    /**
     * @param int $index
     * @return mixed
     */
    public function fetchColumn($index = 0);

    /**
     * @param $mode
     */
    public function setFetchMode($mode);

    /**
     * @return bool
     */
    public function closeCursor();

    /**
     * @param string $parameter
     * @param mixed $value
     * @param $type
     * @return bool
     */
    public function bindValue($parameter, $value, $type = null);

    /**
     * @param $parameter
     * @param $var
     * @param $type
     * @param int $len
     * @return bool
     */
    public function bindParam($parameter, &$var, $type = null, $len = null);

    /**
     * @return string
     */
    public function getErrorCode();

    /**
     * @return array
     */
    public function getErrorInfo();

    /**
     * @param array $parameters
     * @return bool
     */
    public function execute(array $parameters = null);

}