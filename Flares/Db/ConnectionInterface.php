<?php

namespace Flares\Db;

/**
 * Interface ConnectionInterface
 * @package Flares\Db
 * @author Mell <mellevmikhail@gmail.com>
 * @version 1.0.0
 * @copyright 2015 Flares Framework
 */
interface ConnectionInterface
{

    /**
     * @param $string
     * @return StatementInterface
     */
    public function prepare($string);

    /**
     * @return StatementInterface
     */
    public function query();

    /**
     * @param string $string
     * @param int $type
     * @return string
     */
    public function quote($string, $type);

    /**
     * @param string $statement
     * @return int
     */
    public function execute($statement);

    /**
     * @param string $name
     * @return int
     */
    public function getLastInsertId($name);

    /**
     * @return bool
     */
    public function beginTransaction();

    /**
     * @return bool
     */
    public function commit();

    /**
     * @return bool
     */
    public function rollback();

    /**
     * @return bool
     */
    public function inTransaction();

    /**
     * @return string
     */
    public function getErrorCode();

    /**
     * @return array
     */
    public function getErrorInfo();

}