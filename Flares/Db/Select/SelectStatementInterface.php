<?php

namespace Flares\Db\Select;

/**
 * Interface SelectStatementInterface
 * @package Flares\Db\Select
 * @author Mell <mellevmikhail@gmail.com>
 * @version 1.0.0
 * @copyright 2015 Flares Framework
 */
interface SelectStatementInterface
{

    /**
     * @param SelectStatementBuilderInterface $builder
     */
    public function __construct(SelectStatementBuilderInterface $builder);

    /**
     * @return bool
     */
    public function isDistinct();

    /**
     * @return array
     */
    public function getFrom();

    /**
     * @return array
     */
    public function getColumns();

    /**
     * @return string
     */
    public function getWhere();

    /**
     * @return array
     */
    public function getGroupBy();

    /**
     * @return string
     */
    public function getHaving();

    /**
     * @return array
     */
    public function getOrderBy();

    /**
     * @return array
     */
    public function getLimit();

    /**
     * @return array
     */
    public function getJoin();

    /**
     * @return bool
     */
    public function isForUpdate();

    /**
     * @return bool
     */
    public function isSharedLock();

}