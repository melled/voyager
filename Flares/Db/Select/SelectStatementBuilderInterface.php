<?php

namespace Flares\Db\Select;

/**
 * Interface SelectStatementBuilderInterface
 * @package Flares\Db\Select
 * @author Mell <mellevmikhail@gmail.com>
 * @version 1.0.0
 * @copyright 2015 Flares Framework
 */
interface SelectStatementBuilderInterface
{

    const DIRECTION_ASC = 'asc';

    const DIRECTION_DESC = 'desc';

    /**
     * @return SelectStatementInterface
     */
    public function build();

    /**
     * @param string $condition
     * @return $this
     */
    public function setWhere($condition);

    /**
     * @param string $condition
     * @return $this
     */
    public function setOrWhere($condition);

    /**
     * @param string $tableName
     * @return $this
     */
    public function addFrom($tableName);

    /**
     * @param array $tables
     * @return $this
     */
    public function setFrom(array $tables);

    /**
     * @param string $type
     * @param string $table
     * @param string $condition
     * @return $this
     */
    public function addJoin($type, $table, $condition = null);

    /**
     * @param int $limit
     * @param int $offset
     * @return $this
     */
    public function setLimit($limit, $offset = 0);

    /**
     * @param bool $distinct
     * @return $this
     */
    public function setDistinct($distinct = true);

    /**
     * @param string $column
     * @return $this
     */
    public function addColumn($column);

    /**
     * @param array $column
     * @return $this
     */
    public function setColumns(array $column);

    /**
     * @param array $by
     * @param string $direction
     * @return $this
     */
    public function setGroupBy(array $by, $direction = self::DIRECTION_DESC);

    /**
     * @param string $by
     * @param string $direction
     * @return $this
     */
    public function addGroupBy($by, $direction = self::DIRECTION_DESC);

    /**
     * @param string $condition
     * @return $this
     */
    public function addHaving($condition);

    /**
     * @param string $condition
     * @return $this
     */
    public function addOrHaving($condition);

    /**
     * @param array $by
     * @param string $direction
     * @return $this
     */
    public function setOrderBy(array $by, $direction = self::DIRECTION_DESC);

    /**
     * @param string $by
     * @param string $direction
     * @return $this
     */
    public function addOrderBy($by, $direction = self::DIRECTION_ASC);

    /**
     * @param bool $forUpdate
     * @return $this
     */
    public function setForUpdate($forUpdate);

    /**
     * @param bool $sharedLock
     * @return $this
     */
    public function setSharedLock($sharedLock);

    /**
     * @return mixed
     */
    public function getWhere();

    /**
     * @return mixed
     */
    public function getFrom();

    /**
     * @return mixed
     */
    public function getJoin();

    /**
     * @return mixed
     */
    public function getLimit();

    /**
     * @return bool
     */
    public function getDistinct();

    /**
     * @return array
     */
    public function getColumns();

    /**
     * @return mixed
     */
    public function getGroupBy();

    /**
     * @return mixed
     */
    public function getHaving();

    /**
     * @return mixed
     */
    public function getOrderBy();

    /**
     * @return bool
     */
    public function getForUpdate();

    /**
     * @return bool
     */
    public function getSharedLock();
}