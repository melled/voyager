<?php

namespace Flares\Db;

/**
 * Interface DriverInterface
 * @package Flares\Db
 * @author Mell <mellevmikhail@gmail.com>
 * @version 1.0.0
 * @copyright 2015 Flares Framework
 */
interface DriverInterface
{

    /**
     * @param array $parameters
     * @return bool
     */
    public function connect(array $parameters);

    /**
     * @return bool
     */
    public function closeConnection();

    /**
     * @return ConnectionInterface
     */
    public function getConnection();

    /**
     * @param DialectInterface $dialect
     */
    public function setDialect(DialectInterface $dialect);

    /**
     * @return DialectInterface
     */
    public function getDialect();

    /**
     * @return bool
     */
    public function beginTransaction();

    /**
     * @return bool
     */
    public function commit();

    /**
     * @return bool
     */
    public function rollback();

    /**
     * @param string $tableName
     * @return bool
     */
    public function tableExists($tableName);

    /**
     * @param string $tableName
     * @return bool
     */
    public function dropTable($tableName);

    /**
     * @param Table $table
     * @return bool
     */
    public function createTable(Table $table);

    /**
     * @return array
     */
    public function getListTables();

    /**
     * @param string $viewName
     * @return bool
     */
    public function viewExists($viewName);

    /**
     * @param string $viewName
     * @return bool
     */
    public function dropView($viewName);

    /**
     * @param string $viewName
     * @param string $condition
     * @return bool
     */
    public function createView($viewName, $condition);

    /**
     * @return array
     */
    public function getListViews();

    /**
     * @param $select
     * @return mixed
     */
    public function select($select);

    /**
     * @param string $tableName
     * @param array $set
     * @param string $whereCondition
     * @param array $limit
     * @return int
     */
    public function update($tableName, array $set, $whereCondition = null, array $limit = null);

    /**
     * @param string $tableName
     * @param string $whereCondition
     * @param array $limit
     * @return int
     */
    public function delete($tableName, $whereCondition = null, array $limit = null);

    /**
     * @param string $tableName
     * @param array $values
     * @return int
     */
    public function insert($tableName, array $values);

    /**
     * @param string $tableName
     * @param array $columns
     * @param array $values
     * @return int
     */
    public function insertBatch($tableName, array $columns, array $values);

    /**
     * @param string $tableName
     * @param array $columns
     * @return bool
     */
    public function addPrimaryKey($tableName, array $columns);

    /**
     * @param string $tableName
     * @return bool
     */
    public function dropPrimaryKey($tableName);

    /**
     * @param string $tableName
     * @param string $indexName
     * @param array $columns
     * @return bool
     */
    public function addIndex($tableName, $indexName, array $columns);

    /**
     * @param string $tableName
     * @param string $indexName
     * @return bool
     */
    public function dropIndex($tableName, $indexName);

    /**
     * @param string $tableName
     * @return array
     */
    public function showIndexes($tableName);

    /**
     * @param string $tableName
     * @param array $columnNames
     * @param string $referenceTableName
     * @param array $referenceColumnNames
     * @param string $onDelete
     * @return bool
     */
    public function addForeignKey($tableName, $columnNames, $referenceTableName, $referenceColumnNames, $onDelete);

    /**
     * @param string $tableName
     * @param string $foreignKeyName
     * @return bool
     */
    public function dropForeignKey($tableName, $foreignKeyName);

    /**
     * @param string $tableName
     * @param Column $column
     * @return bool
     */
    public function addColumnSql($tableName, Column $column);

    /**
     * @param string $tableName
     * @param Column $column
     * @return bool
     */
    public function modifyColumn($tableName, Column $column);

    /**
     * @param string $tableName
     * @param string $columnName
     * @return bool
     */
    public function dropColumn($tableName, $columnName);

    /**
     * @param string $tableName
     * @param string $columnName
     * @return array
     */
    public function showColumn($tableName, $columnName);

    /**
     * @param string $tableName
     * @return array
     */
    public function describeColumns($tableName);

}