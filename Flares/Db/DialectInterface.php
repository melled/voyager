<?php

namespace Flares\Db;

/**
 * Interface DialectInterface
 * @package Flares\Db
 * @author Mell <mellevmikhail@gmail.com>
 * @version 1.0.0
 * @copyright 2015 Flares Framework
 */
interface DialectInterface
{

    /**
     * @param string $tableName
     * @return string
     */
    public function getDropTableSql($tableName);

    /**
     * @param string $tableName
     * @return string
     */
    public function getTableExistsSql($tableName);

    /**
     * @return string
     */
    public function getListTablesSql();

    /**
     * @param $viewName
     * @param bool $ifExists
     * @return string
     */
    public function getDropViewSql($viewName, $ifExists = false);

    /**
     * @param string $viewName
     * @return string
     */
    public function getViewExistsSql($viewName);

    /**
     * @param Table $table
     * @return string
     */
    public function getCreateTableSql(Table $table);

    /**
     * @param $viewName
     * @param $select
     * @return string
     */
    public function getCreateViewSql($viewName, $select);

    /**
     * @param $select
     * @return string
     */
    public function getSelectSql($select);

    /**
     * @param string $tableName
     * @param $column
     * @return string
     */
    public function getAddColumnSql($tableName, $column);

    /**
     * @param string $tableName
     * @param $column
     * @return string
     */
    public function getModifyColumnSql($tableName, $column);

    /**
     * @param string $tableName
     * @param array $values
     * @return string
     */
    public function getInsertSql($tableName, array $values);

    /**
     * @param string $tableName
     * @param array $columns
     * @param array $values
     * @return string
     */
    public function getInsertBatchSql($tableName, array $columns, array $values);

    /**
     * @param string $tableName
     * @param string $columnName
     * @return string
     */
    public function getDropColumnSql($tableName, $columnName);

    /**
     * @param string $tableName
     * @param string $indexName
     * @return string
     */
    public function getDropIndexSql($tableName, $indexName);

    /**
     * @param string $tableName
     * @param string $foreignKeyName
     * @return string
     */
    public function getDropForeignKeySql($tableName, $foreignKeyName);

    /**
     * @param string $tableName
     * @return string
     */
    public function getDropPrimaryKeySql($tableName);

    /**
     * @param string $query
     * @param int $limit
     * @param int $offset
     * @return string
     */
    public function getLimitSql($query, $limit, $offset = 0);

    /**
     * @param string $tableName
     * @param string $columnName
     * @return string
     */
    public function getShowColumnSql($tableName, $columnName);

    /**
     * @param string $tableName
     * @return string
     */
    public function getDescribeColumnsSql($tableName);

    /**
     * @return string
     */
    public function getListViewsSql();

    /**
     * @param string $tableName
     * @return string
     */
    public function getShowIndexesSql($tableName);

    /**
     * @param string $tableName
     * @param string $indexName
     * @param array $columnNames
     * @return string
     */
    public function getAddIndexSql($tableName, $indexName, array $columnNames);

    /**
     * @param string $tableName
     * @param array $columnNames
     * @return string
     */
    public function getAddPrimaryKeySql($tableName, array $columnNames);

    /**
     * @param string $tableName
     * @param string $columnNames
     * @param string $referenceTableName
     * @param string $referenceColumnNames
     * @param string $onDelete
     * @return string
     */
    public function getAddForeignKeySql($tableName, $columnNames, $referenceTableName, $referenceColumnNames, $onDelete);

    /**
     * @param string $tableName
     * @param string $whereCondition
     * @return string
     */
    public function getDeleteSql($tableName, $whereCondition = null);

    /**
     * @param string $tableName
     * @param array $set
     * @param string $whereCondition
     * @return string
     */
    public function getUpdateSql($tableName, array $set, $whereCondition = null);

}