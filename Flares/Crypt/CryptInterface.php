<?php

namespace Flares\Crypt;

/**
 * Interface CryptInterface
 * @package Flares\Crypt
 */
interface CryptInterface
{
    public function getListAlgorithms();

    public function getListModes();

    public function getKey();

    public function setKey($key);

    public function getMode();

    public function setMode($mode);

    public function setAlgorithm($algorithm);

    public function getAlgorithm();

    public function encrypt($value, $key = null);

    public function decrypt($value, $key = null);

    public function encryptBase64($value, $key = null);

    public function decryptBase64($value, $key = null);
}