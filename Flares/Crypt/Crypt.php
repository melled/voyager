<?php

namespace Flares\Crypt;

/**
 * Class Crypt
 * @package Flares\Crypt
 */
class Crypt implements CryptInterface
{
    private $key = '1a46f65de00e683b42a11624ef6b7e6d';

    private $mode = MCRYPT_MODE_ECB;

    private $algorithm = MCRYPT_RIJNDAEL_256;

    public function getListAlgorithms()
    {
        return mcrypt_list_algorithms();
    }

    public function getListModes()
    {
        return mcrypt_list_modes();
    }

    public function getKey()
    {
        return $this->key;
    }

    public function setKey($key)
    {
        $this->key = $key;
    }

    public function getMode()
    {
        return $this->mode;
    }

    public function setMode($mode)
    {
        $this->mode = $mode;
    }

    public function setAlgorithm($algorithm)
    {
        $this->algorithm = $algorithm;
    }

    public function getAlgorithm()
    {
        return $this->algorithm;
    }

    public function encrypt($value, $key = null, $base64 = false)
    {
        $mode = $this->mode;
        $algorithm = $this->algorithm;
        $iv = $this->createIv();

        if ($key === null) {
            $key = $this->key;
        }

        $value = $this->addPadding($value);

        $data = mcrypt_encrypt($algorithm, $key, $value, $mode, $iv);

        return $data;
    }

    public function decrypt($value, $key = null, $base64 = false)
    {
        $mode = $this->mode;
        $algorithm = $this->algorithm;
        $iv = $this->createIv();

        if ($key === null) {
            $key = $this->key;
        }

        $data = mcrypt_decrypt($algorithm, $key, $value, $mode, $iv);

        return $this->removePadding($data);
    }

    public function encryptBase64($value, $key = null)
    {
        return base64_encode($this->encrypt($value, $key));
    }

    public function decryptBase64($value, $key = null)
    {
        return $this->decrypt(base64_decode($value), $key);
    }

    protected function addPadding($value)
    {
        $pad = $this->getIvSize() - (strlen($value) % $this->getIvSize());
        return $value . str_repeat(chr($pad), $pad);
    }

    protected function removePadding($value)
    {
        $pad = ord($value[($len = strlen($value)) - 1]);
        return substr($value, 0, $len - $pad);
    }

    protected function getIvSize()
    {
        return mcrypt_get_iv_size($this->algorithm, $this->mode);
    }

    protected function createIv()
    {
        return mcrypt_create_iv(
            $this->getIvSize(),
            $this->getRandomSource()
        );
    }

    protected function getRandomSource()
    {
        if (defined('MCRYPT_DEV_URANDOM')) {
            return MCRYPT_DEV_URANDOM;
        }

        if (defined('MCRYPT_DEV_RANDOM')) {
            return MCRYPT_DEV_RANDOM;
        }

        return MCRYPT_RAND;
    }
}