<?php

namespace Flares\Mail;

use Flares\Storage\StorageInterface;

/**
 * Class Message
 * @package Flares\Mail
 */
class Message implements MessageInterface
{
    /**
     * @var Mail
     */
    protected $mail;

    protected $from;

    protected $to = [];

    protected $subject = '';

    protected $cc = [];

    protected $bcc = [];

    protected $priority;

    protected $replyTo = [];

    protected $body = '';

    protected $bodyHtml = false;

    protected $attachment = [];

    /**
     * @param Mail $mailer
     */
    public function __construct(Mail $mailer)
    {
        $this->mail = $mailer;
    }

    /**
     * @param string $address
     * @param string $name
     * @return $this
     */
    public function setFrom($address, $name = null)
    {
        $this->from = [
            'address' => $address,
            'name' => $name
        ];

        return $this;
    }

    /**
     * @param string $address
     * @param string $name
     * @return $this
     */
    public function addTo($address, $name = null)
    {
        $this->to[] = [
            'address' => $address,
            'name' => $name
        ];

        return $this;
    }

    /**
     * @param string $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @param string $address
     * @param string $name
     * @return $this
     */
    public function addReplyTo($address, $name = null)
    {
        $this->replyTo[] = [
            'address' => $address,
            'name' => $name
        ];

        return $this;
    }

    /**
     * @param int $priority
     * @return $this
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;
        return $this;
    }

    /**
     * @param string $address
     * @param string $name
     * @return $this
     */
    public function addBcc($address, $name = null)
    {
        $this->bcc[] = [
            'address' => $address,
            'name' => $name
        ];

        return $this;
    }

    /**
     * @param string $address
     * @param string $name
     * @return $this
     */
    public function addCc($address, $name = null)
    {
        $this->cc[] = [
            'address' => $address,
            'name' => $name
        ];

        return $this;
    }

    /**
     * @param string $body
     * @return $this
     */
    public function setBody($body, $isHtml = false)
    {
        $this->body = $body;
        $this->bodyHtml = $isHtml;
        return $this;
    }

    /**
     * @param StorageInterface $storage
     * @param string $path
     * @return $this
     */
    public function addAttachment(StorageInterface $storage, $path)
    {
        $this->attachment[] = [
            'storage' => $storage,
            'path' => $path
        ];

        return $this;
    }

    /**
     * @return string
     */
    public function getFrom()
    {
        return $this->from;
    }

    /**
     * @return array
     */
    public function getTo()
    {
        return $this->to;
    }

    /**
     * @return array
     */
    public function getBcc()
    {
        return $this->bcc;
    }

    /**
     * @return array
     */
    public function getCc()
    {
        return $this->cc;
    }

    /**
     * @return int
     */
    public function getPriority()
    {
        return $this->priority;
    }

    /**
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * @return array
     */
    public function getReplyTo()
    {
        return $this->replyTo;
    }

    /**
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @return bool
     */
    public function isHtml()
    {
        return $this->bodyHtml;
    }

    /**
     * @return array
     */
    public function getAttachment()
    {
        return $this->attachment;
    }

    /**
     * @return bool
     */
    public function hasAttachment()
    {
        return $this->attachment !== [];
    }

    public function send()
    {
        return $this->mail->sendMail($this);
    }
}