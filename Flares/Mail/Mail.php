<?php

namespace Flares\Mail;

class Mail implements MailerInterface
{
    protected $host;

    protected $smtpConnection;

    protected $response;

    protected $delimiter;

    protected $auth = false;

    protected $ssl;

    public function __construct()
    {
        //$this->delimiter = md5(mt_rand());
    }

    /**
     * @param string $host
     * @param int $port
     * @param int $timeOut
     * @throws Exception
     */
    public function connect($host, $port = 25, $ssl = false, $timeOut = 10)
    {
        if ($this->isConnected()) {
            throw new Exception('Already connected');
        }

        $this->host = $host;
        $this->ssl = $ssl;

        if ($ssl) {
            $host = 'ssl://' . $host;
        }

        $errno = null;
        $errstr = null;

        $connection = @fsockopen(
            $host,
            $port,
            $errno,
            $errstr,
            $timeOut
        );

        if (!$connection) {
            throw new Exception('Connection Error: ' . "[{$errno}] $errstr");
        }

        $this->smtpConnection = $connection;
        $this->readFromConnection();
    }

    /**
     * @return bool
     */
    public function isConnected()
    {
        if ($this->smtpConnection === null) {
            return false;
        }

        $meta = stream_get_meta_data($this->smtpConnection);

        if ($meta['eof']) {
            $this->closeConnection();
            return false;
        }

        return true;
    }

    public function closeConnection()
    {
        if ($this->smtpConnection !== null) {
            fclose($this->smtpConnection);
            $this->smtpConnection = null;
        }

        $this->lastResponseCode = null;
        $this->lastResponse = null;
        $this->host = null;
        $this->auth = false;
    }

    /**
     * @param string $login
     * @param string $password
     * @throws Exception
     */
    public function auth($login, $password)
    {
        if (!$this->isConnected()) {
            throw new Exception('No connection');
        }

        $this->sendRequest('EHLO user');

        $this->sendRequest('AUTH LOGIN');
        if ($this->getResponse() !== 334) {
            throw new Exception('Auth failed');
        }

        $this->sendRequest($login, true);
        if ($this->getResponse() !== 334) {
            throw new Exception('Auth failed');
        }

        $this->sendRequest($password, true);
        if ($this->getResponse() !== 235) {
            throw new Exception('Auth failed');
        }

        $this->auth = true;
    }

    public function createMessage()
    {
        return new Message($this);
    }

    public function sendMessage(Message $message)
    {
        if (!$this->auth) {
            throw new Exception('Not authenticated');
        }


        $this->sendRequest('MAIL FROM: <' . $message->getFrom()['address'] . '>');
        if ($this->getResponse() !== 250) {
            throw new Exception('Send failed');
        }

        $this->sendRequest('RCPT TO:' . $this->compileMultipleAddress($message->getTo()));
        if ($this->getResponse() !== 250 && $this->getResponse() !== 251) {
            throw new Exception('Send failed');
        }

        $this->sendRequest('DATA');
        if ($this->getResponse() !== 354) {
            throw new Exception('Send failed');
        }

        $this->sendRequest($this->compileMessage($message));
        vardump($this->getResponse());
        if ($this->getResponse() !== 250) {
            throw new Exception('Send failed');
        }

        //$this->sendRequest('QUIT');
        //vardump($this->getResponse());
    }

    protected function compileMessage(Message $message)
    {
        $headers = $this->compileHeaders($message);
        $body = $this->compileBody($message);
        return $headers . $body;
    }

    protected function compileHeaders(Message $message)
    {
        $headers = [];

        if ($message->getPriority()) {
            $headers['X-Priority'] = $message->getPriority();
        }

        if ($message->getFrom()) {
            $headers['From'] = $this->compileAddress($message->getFrom());
        }

        if ($message->getBcc() !== []) {
            $headers['Bcc'] = $this->compileMultipleAddress($message->getBcc());
        }

        if ($message->getCc() !== []) {
            $headers['Cc'] = $this->compileMultipleAddress($message->getCc());
        }

        if ($message->getReplyTo() !== []) {
            $headers['Reply-To'] = $this->compileMultipleAddress($message->getReplyTo());
        }

        if ($message->getTo() !== []) {
            $headers['To'] = $this->compileMultipleAddress($message->getTo());
        }

        if ($message->getSubject()) {
            $headers['Subject'] = $this->encodeString($message->getSubject());
        }

        $headers['MIME-Version'] = '1.0';

        if ($message->hasAttachment()) {
            $headers['Content-Type'] = 'multipart/mixed; boundary="--' . $this->delimiter . '"';
        } else {
            if ($message->isHtml()) {
                $headers['Content-Type'] = 'text/html; charset=utf-8';
            } else {
                $headers['Content-Type'] = 'text/plain; charset=utf-8';
            }
            $headers['Content-Transfer-Encoding'] = '8bit';
        }

        $resultHeaders = '';

        foreach ($headers as $name => $value ) {
            $resultHeaders .= "$name: $value\r\n";
        }

        return $resultHeaders . "\r\n";
    }

    protected function compileBody(Message $message)
    {
        if (!$message->hasAttachment()) {
            return $message->getBody();
        }

        $result = '';

        $result .= "--$this->delimiter\r\n";

        if ($message->getBody()) {
            if ($message->isHtml()) {
                $result .= "Content-Type: text/html; charset=utf-8\r\n";
            } else {
                $result .= "Content-Type: text/plain; charset=utf-8\r\n";
            }
            $result .= "Content-Transfer-Encoding: 8bit\r\n\r\n";

            $result .= $message->getBody() . "\r\n";
        }

        foreach ($message->getAttachment() as $attachment) {
            $storage = $attachment['storage'];
            $file = $attachment['path'];

            if (!$storage->exists($file)) {
                throw new Exception('File not found');
            }

            $fileCode = chunk_split(base64_encode($storage->getFile($file)));

            $result .= "\r\n--$this->delimiter\r\n";

            $result .= 'Content-Type: application/octet-stream; name="' . $storage->getFileName($file) . "\"\r\n";
            $result .= 'Content-transfer-encoding: base64' . "\r\n";
            $result .= 'Content-Disposition: attachment; filename="' . $storage->getFileName($file) . "\"\r\n\r\n";
            $result .= $fileCode;
        }

        $result .= "\r\n--$this->delimiter--";

        return $result;

    }

    protected function compileAddress(array $address)
    {
        $result = '';
        if ($address['name']) {
            $result .= $this->encodeString($address['name']);
        }
        $result .= '<' . $address['address'] . '>';
        return $result;
    }

    protected function compileMultipleAddress(array $values)
    {
        $result = [];
        foreach ($values as $value) {
            $result[] = $this->compileAddress($value);
        }

        return implode(', ', $result);
    }

    protected function encodeString($string)
    {
        return '=?UTF-8?B?' . base64_encode($string) . '?=';
    }

    protected function sendRequest($value, $base64 = false, $crlf = true)
    {
        if ($base64) {
            $value = base64_encode($value);
        }

        if ($crlf) {
            $value .= "\r\n";
        }

        $this->putToConnection($value);
        $this->response = (int)$this->readFromConnection();
    }

    protected function putToConnection($value)
    {
        fwrite($this->smtpConnection, $value);
    }

    protected function readFromConnection()
    {
        $data = '';
        while($str = fgets($this->smtpConnection, 515))
        {
            $data .= $str;
            if(substr($str,3,1) === ' ') {
                break;
            }
        }
        return $data;
    }
    
    protected function getResponse()
    {
        return $this->response;
    }
}