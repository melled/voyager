<?php

namespace Flares\Logging;

/**
 * Interface FormatterInterface
 * @package Flares\Logging
 */
interface FormatterInterface
{
    public function format(LogEntry $entry);
}