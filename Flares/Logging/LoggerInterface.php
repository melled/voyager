<?php

namespace Flares\Logging;

/**
 * Interface LoggerInterface
 * @package Flares\Logging
 */
interface LoggerInterface
{
    const CRITICAL = 0;
    const ERROR = 1;
    const WARNING = 2;
    const ALERT = 3;
    const NOTICE = 4;
    const INFO = 5;
    const DEBUG = 6;
    const EMERGENCY = 7;

    /**
     * @param string $message
     * @param array $context
     */
    public function critical($message, array $context = []);

    /**
     * @param string $message
     * @param array $context
     */
    public function error($message, array $context = []);

    /**
     * @param string $message
     * @param array $context
     */
    public function warning($message, array $context = []);

    /**
     * @param string $message
     * @param array $context
     */
    public function alert($message, array $context = []);

    /**
     * @param string $message
     * @param array $context
     */
    public function notice($message, array $context = []);

    /**
     * @param string $message
     * @param array $context
     */
    public function info($message, array $context = []);

    /**
     * @param string $message
     * @param array $context
     */
    public function debug($message, array $context = []);

    /**
     * @param string $message
     * @param array $context
     */
    public function emergency($message, array $context = []);

    /**
     * @param int $level
     * @param string $message
     * @param array $context
     */
    public function log($level, $message, array $context = []);

    public function beginTransaction();

    public function commitTransaction();
}