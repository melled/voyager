<?php

namespace Flares\Logging\Adapter;

use Flares\Logging\Exception;
use Flares\Logging\LogEntry;
use Flares\Logging\AbstractLogger;
use Flares\Storage\StorageAdapterInterface;

/**
 * Class File
 * @package Flares\Logging\Adapter
 */
class File extends AbstractLogger
{
    /**
     * @var string
     */
    protected $fileName;

    /**
     * @var StorageAdapterInterface
     */
    protected $storage;

    /**
     * @param string $fileName
     * @param StorageAdapterInterface $storage
     */
    public function __construct($fileName, StorageAdapterInterface $storage)
    {
        $this->fileName = $fileName;
        $this->storage = $storage;
    }

    /**
     * @param LogEntry $entry
     * @throws Exception
     */
    protected function writeLog(LogEntry $entry)
    {
        $this->storage->appendToFile(
            $this->fileName,
            $this->formatter->format($entry)
        );
    }
}