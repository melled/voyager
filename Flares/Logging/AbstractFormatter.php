<?php

namespace Flares\Logging;

/**
 * Class AbstractFormatter
 * @package Flares\Logging
 */
abstract class AbstractFormatter implements FormatterInterface
{
    private static $levelNames = [
        LoggerInterface::ALERT => 'ALERT',
        LoggerInterface::CRITICAL => 'CRITICAL',
        LoggerInterface::DEBUG => 'DEBUG',
        LoggerInterface::EMERGENCY => 'EMERGENCY',
        LoggerInterface::ERROR => 'ERROR',
        LoggerInterface::ALERT => 'ALERT',
        LoggerInterface::INFO => 'INFO',
        LoggerInterface::NOTICE => 'NOTICE',
        LoggerInterface::WARNING => 'WARNING'
    ];

    protected function getLevelName($level)
    {
        return self::$levelNames[$level];
    }
}