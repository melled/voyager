<?php

namespace Flares\Logging;

/**
 * Class AbstractLogger
 * @package Flares\Logging
 */
abstract class AbstractLogger implements LoggerInterface
{
    protected $transaction = false;

    /**
     * @var LogEntry[]
     */
    protected $logs = [];

    /**
     * @var FormatterInterface
     */
    protected $formatter;

    /**
     * @param string $message
     * @param array $context
     * @throws Exception
     */
    public function critical($message, array $context = [])
    {
        $this->log(self::CRITICAL, $message, $context);
    }

    /**
     * @param string $message
     * @param array $context
     * @throws Exception
     */
    public function error($message, array $context = [])
    {
        $this->log(self::ERROR, $message, $context);
    }

    /**
     * @param string $message
     * @param array $context
     * @throws Exception
     */
    public function warning($message, array $context = [])
    {
        $this->log(self::WARNING, $message, $context);
    }

    /**
     * @param string $message
     * @param array $context
     * @throws Exception
     */
    public function alert($message, array $context = [])
    {
        $this->log(self::ALERT, $message, $context);
    }

    /**
     * @param string $message
     * @param array $context
     * @throws Exception
     */
    public function notice($message, array $context = [])
    {
        $this->log(self::NOTICE, $message, $context);
    }

    /**
     * @param string $message
     * @param array $context
     * @throws Exception
     */
    public function info($message, array $context = [])
    {
        $this->log(self::INFO, $message, $context);
    }

    /**
     * @param string $message
     * @param array $context
     * @throws Exception
     */
    public function debug($message, array $context = [])
    {
        $this->log(self::DEBUG, $message, $context);
    }

    /**
     * @param string $message
     * @param array $context
     * @throws Exception
     */
    public function emergency($message, array $context = [])
    {
        $this->log(self::EMERGENCY, $message, $context);
    }

    /**
     * @param int $level
     * @param string $message
     * @param array $context
     * @throws Exception
     */
    public function log($level, $message, array $context = [])
    {
        if ($level < 0 || $level > 7) {
            throw new Exception('Incorrect log level');
        }

        $time = new \DateTime();

        $logEntry = new LogEntry($time, $level, $message, $context);

        if ($this->transaction) {
            $this->logs[] = $logEntry;
        } else {
            $this->writeLog($logEntry);
        }

    }

    public function beginTransaction()
    {
        $this->transaction = true;
    }

    /**
     * @throws Exception
     */
    public function commitTransaction()
    {
        if (!$this->transaction) {
            throw new Exception('Transaction is not started');
        }

        foreach ($this->logs as $entry) {
            $this->writeLog($entry);
        }

        $this->transaction = false;
    }

    /**
     * @param FormatterInterface $formatter
     */
    public function setFormatter(FormatterInterface $formatter)
    {
        $this->formatter = $formatter;
    }

    /**
     * @return FormatterInterface
     */
    public function getFormatter()
    {
        return $this->formatter;
    }

    /**
     * @param LogEntry $entry
     */
    abstract protected function writeLog(LogEntry $entry);
}