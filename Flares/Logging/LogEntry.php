<?php

namespace Flares\Logging;

/**
 * Class LogEntry
 * @package Flares\Logging
 */
class LogEntry
{
    protected $time;

    protected $message;

    protected $logLevel;

    protected $context;

    /**
     * @param \DateTime $time
     * @param int $logLevel
     * @param string $message
     * @param array $context
     */
    public function __construct($time, $logLevel, $message, array $context = [])
    {
        $this->time = $time;
        $this->logLevel = $logLevel;
        $this->message = $message;
        $this->context = $context;
    }

    /**
     * @return \DateTime
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * @return int
     */
    public function getLogLevel()
    {
        return $this->logLevel;
    }

    /**
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @return array
     */
    public function getContext()
    {
        return $this->context;
    }
}