<?php

namespace Flares\Logging\Formatter;

use Flares\Logging\AbstractFormatter;
use Flares\Logging\LogEntry;

/**
 * Class Line
 * @package Flares\Logging\Formatter
 */
class Line extends AbstractFormatter
{
    protected $entryFormat = '[%date%][%type%] %message%';

    protected $dateFormat = 'Y-m-d H:i:s';

    public function __construct($format = null, $dateTimeFormat = null)
    {
        if ($format === null) {
            $this->format = $format;
        }

        if ($dateTimeFormat === null) {
            $this->dateTimeFormat = $dateTimeFormat;
        }
    }

    public function format(LogEntry $entry)
    {
        $format = $this->entryFormat;
        $date = $entry->getTime()->format($this->dateFormat . '');
        $type = $this->getLevelName($entry->getLogLevel());
        $format = str_replace('%date%', $date, $format);
        $format = str_replace('%type%', $type, $format);
        $format = str_replace('%message%', $entry->getMessage(), $format) . PHP_EOL;
        return $format;
    }
}