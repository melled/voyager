<?php

namespace Flares\Cache\Memcache;

/**
 * Interface DriverInterface
 * @package Flares\Cache\Memcache
 */
interface DriverInterface
{
    /**
     * @param array $options
     */
    public function __construct(array $options);

    /**
     * @return bool
     */
    public function connect();
}