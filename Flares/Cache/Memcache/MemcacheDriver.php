<?php

namespace Flares\Cache\Memcache;

use Flares\Cache\AbstractCacheStore;

/**
 * Class MemcacheDriver
 * @package Flares\Cache\Memcache
 */
class MemcacheDriver extends AbstractCacheStore implements DriverInterface
{
    protected $memcache;

    protected $options = [
        'host' => 'localhost',
        'port' => 11211,
        'persistent' => true,
        'weight' => null
    ];

    public function __construct(array $options = [])
    {
        $this->memcache = new \Memcache();
        $this->options = array_merge($this->options, $options);
    }

    public function connect()
    {
        $host = $this->options['host'];
        $port = $this->options['port'];
        $persistent = $this->options['persistent'];
        $weight = $this->options['port'];
        $this->memcache->addserver($host, $port, $persistent, $weight);
    }

    public function get($key, $default = false)
    {
        $value = $this->memcache->get($key);
        if ($value === false) {
            $value = $this->getRealValue($default);
        }
        return $value;
    }

    public function remove($key)
    {
        return $this->memcache->delete($key);
    }

    public function has($key)
    {
        $value = $this->memcache->get($key);
        return $value !== false;
    }

    public function add($key, $value, $lifetime = 0)
    {
        $value = $this->getRealValue($value);
        return $this->memcache->add($key, $value, false, $lifetime);
    }

    public function set($key, $value, $lifetime = 0)
    {
        $value = $this->getRealValue($value);
        return $this->memcache->set($key, $value, false, $lifetime);
    }

    public function flush()
    {
        return $this->memcache->flush();
    }

    public function increment($key, $step = 1)
    {
        return $this->memcache->increment($key, $step);
    }

    public function decrement($key, $step = 1)
    {
        return $this->memcache->decrement($key, $step);
    }
}