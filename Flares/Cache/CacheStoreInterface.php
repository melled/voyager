<?php

namespace Flares\Cache;

/**
 * Interface CacheStoreInterface
 * @package Flares\Cache
 */
interface CacheStoreInterface
{
    /**
     * @param string $key
     * @param mixed $value
     * @param int $lifetime
     * @return mixed
     */
    public function remember($key, $value, $lifetime = 0);

    /**
     * @param string $key
     * @param mixed $value
     * @return mixed
     */
    public function rememberForever($key, $value);

    /**
     * @param string $key
     * @return mixed
     */
    public function pull($key);

    /**
     * @param string $key
     * @param mixed $value
     * @return bool
     */
    public function forever($key, $value);

    /**
     * @param string $key
     * @param bool|false $default
     * @return mixed
     */
    public function get($key, $default = false);

    /**
     * @param string $key
     * @return bool
     */
    public function remove($key);

    /**
     * @param string $key
     * @return bool
     */
    public function has($key);

    /**
     * @param string $key
     * @param mixed $value
     * @param int $lifetime
     * @return bool
     */
    public function add($key, $value, $lifetime = 0);

    /**
     * @param string $key
     * @param mixed $value
     * @param int $lifetime
     * @return bool
     */
    public function set($key, $value, $lifetime = 0);

    /**
     * @return bool
     */
    public function flush();

    /**
     * @param string $key
     * @param int $step
     * @return bool
     */
    public function increment($key, $step = 1);

    /**
     * @param string $key
     * @param int $step
     * @return bool
     */
    public function decrement($key, $step = 1);
}