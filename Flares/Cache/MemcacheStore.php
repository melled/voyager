<?php

namespace Flares\Cache;

use Flares\Cache\Memcache\MemcacheDriver;
use Flares\DI\Container;

/**
 * Class MemcacheStore
 * @package Flares\Cache
 */
class MemcacheStore extends AbstractCacheStore
{
    /**
     * @var MemcacheDriver
     */
    protected $driver;

    /**
     * @var Container
     */
    protected $di;

    /**
     * @param Container $di
     */
    public function __construct(Container $di)
    {
        $this->di = $di;
        if ($di->hasService('memcache_driver')) {
            $this->driver = $di->make('memcache_driver');
        }
    }

    public function get($key, $default = false)
    {
        return $this->driver->get($key, $default);
    }

    public function remove($key)
    {
        return $this->driver->remove($key);
    }

    public function has($key)
    {
        return $this->driver->has($key);
    }

    public function add($key, $value, $lifetime = 0)
    {
        return $this->driver->add($key, $value, $lifetime);
    }

    public function set($key, $value, $lifetime = 0)
    {
        return $this->set($key, $value, $lifetime);
    }

    public function flush()
    {
        return $this->driver->flush();
    }

    public function increment($key, $step = 1)
    {
        return $this->driver->increment($key, $step);
    }

    public function decrement($key, $step = 1)
    {
        return $this->decrement($key, $step);
    }
}