<?php

namespace Flares\Cache;

/**
 * Class AbstractCacheStore
 * @package Flares\Cache
 */
abstract class AbstractCacheStore implements CacheStoreInterface
{
    public function remember($key, $value, $lifetime = 0)
    {
        if ($this->has($key)) {
            return $this->get($key);
        }
        $this->add($key, $value, $lifetime);
        return $this->getRealValue($value);
    }

    public function rememberForever($key, $value)
    {
        $this->remember($key, $value);
    }

    public function pull($key)
    {
        $value = $this->get($key);
        $deleteResult = $this->remove($key);
        return $deleteResult ? $value : false;
    }

    public function forever($key, $value)
    {
        $this->set($key, $value);
    }

    protected function getRealValue($value)
    {
        if ($value instanceof \Closure) {
            return call_user_func($value);
        }
        return $value;
    }
}