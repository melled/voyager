<?php

namespace Flares\Localization;

/**
 * Class Translator
 * @package Flares\Localization
 */
class Translator implements TranslatorInterface
{
    protected $locale;

    protected $translations;

    /**
     * @param array $translations
     * @param string $locale
     */
    public function __construct(array $translations, $locale = null)
    {
        $this->translations = $translations;
        $this->locale = $locale;
    }

    /**
     * @param string $locale
     */
    public function setLocale($locale)
    {
        $this->locale;
    }

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $key
     * @param array $placeholders
     * @return string
     * @throws Exception
     */
    public function translate($key, $placeholders = null)
    {
        if (!$this->exists($key)) {
            throw new Exception('Translation key does not exist');
        }

        $translation = $this->translations[$key];

        if (is_array($placeholders)) {
            $args = array_merge([$translation], $placeholders);
            $args = array_values($args);

            $translation = call_user_func_array(
                'sprintf',
                $args
            );
        }

        return $translation;
    }

    /**
     * @param string $key
     * @param array $placeholders
     * @return string
     */
    public function _($key, $placeholders = null)
    {
        return $this->translate($key, $placeholders);
    }

    /**
     * @param string $key
     * @return bool
     */
    public function exists($key)
    {
        return isset($this->translations[$key]);
    }
}