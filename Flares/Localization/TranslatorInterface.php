<?php

namespace Flares\Localization;

/**
 * Interface TranslatorInterface
 * @package Flares\Translation
 */
interface TranslatorInterface
{
    /**
     * @param string $locale
     */
    public function setLocale($locale);

    /**
     * @return string
     */
    public function getLocale();

    /**
     * @param string $key
     * @param array $placeholders
     * @return string
     */
    public function translate($key, $placeholders = null);

    /**
     * @param string $key
     * @param array $placeholders
     * @return string
     */
    public function _($key, $placeholders = null);

    /**
     * @param string $key
     * @return bool
     */
    public function exists($key);
}