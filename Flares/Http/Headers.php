<?php

namespace Flares\Http;

/**
 * Class Headers
 * @package Flares\Http
 */
class Headers implements \IteratorAggregate
{
    protected $headers;

    /**
     * @param array $headers
     */
    public function __construct(array $headers = [])
    {
        $this->headers = $headers;
    }

    /**
     * @param string $name
     * @param string $value
     */
    public function add($name, $value)
    {
        $name = strtolower($name);
        $this->headers[$name][] = $value;
    }

    /**
     * @param string $name
     * @param $defaultValue
     * @return mixed
     */
    public function get($name, $defaultValue)
    {
        $name = strtolower($name);
        if ($this->has($name)) {
            return $this->headers[$name];
        }
        return $defaultValue;
    }

    /**
     * @param string $name
     * @param string $value
     */
    public function set($name, $value = '')
    {
        $name = strtolower($name);
        $this->headers[$name] = (array) $value;
    }

    /**
     * @param string $name
     * @return bool
     */
    public function has($name)
    {
        $name = strtolower($name);
        return isset($this->headers[$name]);
    }

    /**
     * @param string $name
     */
    public function remove($name)
    {
        $name = strtolower($name);
        unset($this->headers[$name]);
    }

    public function removeAll()
    {
        $this->headers = [];
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->headers);
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->headers);
    }
}