<?php

namespace Flares\Http\File;

/**
 * Class File
 * @package Flares\Http\File
 */
class File
{
    protected $file;

    /**
     * @param array $file
     */
    public function __construct(array $file)
    {
        $this->file = $file;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->getFileProperty('name');
    }

    /**
     * @return string
     */
    public function getSize()
    {
        return $this->getFileProperty('size');
    }

    /**
     * @return string
     */
    public function hasError()
    {
        return $this->getFileProperty('error');
    }

    /**
     * @param string $path
     * @return bool
     */
    public function moveFile($path)
    {
        return move_uploaded_file($this->getTempName(), $path);
    }

    /**
     * @return string
     */
    public function getTempName()
    {
        return $this->getFileProperty('tmp_name');
    }

    /**
     * @return string
     */
    public function getType()
    {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        return finfo_file($finfo, $this->getFileProperty('tmp_name'));
    }

    /**
     * @param string $name
     * @return string
     */
    private function getFileProperty($name)
    {
        if (isset($this->file[$name])) {
            return $this->file[$name];
        }

        return null;
    }
}