<?php

namespace Flares\Http\File;

/**
 * Class FileCollection
 * @package Flares\Http\File
 */
class FileCollection implements \IteratorAggregate
{
    protected $files;

    /**
     * @param array $files
     */
    public function __construct(array $files = null)
    {
        $files = $files === null ? $_FILES : $files;
        foreach ($files as $name => $file) {
            $this->files[$name] = new File($file);
        }
    }

    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->files);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function has($name)
    {
        return isset($this->files[$name]);
    }

    /**
     * @param string $name
     * @return File
     */
    public function get($name = null)
    {
        if ($name === null) {
            return $this->files;
        }

        return isset($this->files[$name]) ? $this->files[$name] : null;
    }

    public function count()
    {
        return count($this->files);
    }
}