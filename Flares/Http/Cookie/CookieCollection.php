<?php

namespace Flares\Http\Cookie;

use Flares\Http\Exception;

/**
 * Class CookieCollection
 * @package Flares\Http\Cookie
 */
class CookieCollection implements \IteratorAggregate
{
    /**
     * @var array
     */
    protected $cookies = [];

    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator($this->cookies);
    }

    /**
     * @param array $cookies
     */
    public function __construct(array $cookies = [])
    {
        foreach ($cookies as $cookie) {
            $this->cookies[$cookie->getName] = $cookie;
        }
    }

    /**
     * @param $name
     * @return bool
     */
    public function has($name)
    {
        return isset($this->cookies[$name]);
    }

    /**
     * @param string $name
     */
    public function remove($name)
    {
        unset($this->cookies[$name]);
        setcookie($name, '', 1);
    }

    /**
     * @param string $name
     * @return Cookie
     * @throws Exception
     */
    public function get($name)
    {
        if (!$this->has($name)) {
            throw new Exception('Cookie not found');
        }

        return $this->cookies[$name];
    }

    /**
     * @param Cookie $cookie
     */
    public function add(Cookie $cookie)
    {
        $this->cookies[$cookie->getName()] = $cookie;
    }
}