<?php

namespace Flares\Http\Cookie;

/**
 * Class Cookie
 * @package Flares\Http\Cookie
 */
class Cookie
{
    protected $name;

    protected $value;

    protected $expire = 0;

    protected $domain = '';

    protected $path = '/';

    protected $secure = false;

    protected $httpOnly = false;

    public function __construct($name, $value = null, $expire = 0)
    {
        $this->name = $name;
        $this->value = $value;
        $this->expire = $expire;
    }

    /**
     * @param mixed $defaultValue
     * @return mixed
     */
    public function getValue($defaultValue = null)
    {
        if ($this->value === null) {
            return $defaultValue;
        }
        return $this->value;
    }

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @param int $expire
     */
    public function setExpire($expire)
    {
        $this->expire = $expire;
    }

    /**
     * @return int
     */
    public function getExpire()
    {
        return $this->expire;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $domain
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;
    }

    /**
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param bool $httpOnly
     */
    public function setHttpOnly($httpOnly)
    {
        $this->httpOnly = $httpOnly;
    }

    /**
     * @return bool
     */
    public function getHttpOnly()
    {
        return $this->httpOnly;
    }

    /**
     * @param bool $secure
     */
    public function setSecure($secure)
    {
        $this->secure = $secure;
    }

    /**
     * @return bool
     */
    public function getSecure()
    {
        return $this->secure;
    }
}