<?php

namespace Flares\Http;

use Flares\Http\File\FileCollection;

/**
 * Interface RequestInterface
 * @package Flares\Http
 */
interface RequestInterface
{
    /**
     * @return string
     */
    public function getMethod();

    /**
     * @return bool
     */
    public function isAjax();

    /**
     * @return bool
     */
    public function isGet();

    /**
     * @return bool
     */
    public function isPost();

    /**
     * @return bool
     */
    public function isDelete();

    /**
     * @return bool
     */
    public function isPut();

    /**
     * @param string $name
     * @param string $defaultValue
     * @return array|string
     */
    public function getRequest($name = null, $defaultValue = null);

    /**
     * @param string $name
     * @param string $defaultValue
     * @return array|string
     */
    public function get($name = null, $defaultValue = null);

    /**
     * @param string $name
     * @param string $defaultValue
     * @return array|string
     */
    public function getPost($name = null, $defaultValue = null);

    /**
     * @param string $name
     * @param string $defaultValue
     * @return array|string
     */
    public function getPut($name = null, $defaultValue = null);

    /**
     * @param string $name
     * @param string $defaultValue
     * @return string|array
     */
    public function getDelete($name = null, $defaultValue = null);

    /**
     * @param string $name
     * @return bool
     */
    public function hasRequest($name);

    /**
     * @param string $name
     * @return bool
     */
    public function hasGet($name);

    /**
     * @param string $name
     * @return bool
     */
    public function hasPost($name);

    /**
     * @param string $name
     * @return bool
     */
    public function hasPut($name);
    /**
     * @param string $name
     * @return bool
     */
    public function hasDelete($name);

    /**
     * @return bool
     */
    public function hasFiles();

    /**
     * @return FileCollection
     */
    public function getFiles();

    /**
     * @return Headers
     */
    public function getHeaders();

    /**
     * @return string
     */
    public function getRequestUrl();

    /**
     * @return string
     */
    public function getScheme();

    /**
     * @return string
     */
    public function getQueryString();

    /**
     * @return string
     */
    public function getScriptName();

    /**
     * @return string
     */
    public function getRemoteIp();
}