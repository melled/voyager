<?php

namespace Flares\Http;

use Flares\Http\File\FileCollection;

/**
 * Class Request
 * @package Flares\Http
 */
class Request
{
    protected $server;

    protected $request;

    /**
     * @var FileCollection
     */
    protected $files;

    /**
     * @var Headers
     */
    protected $headers;

    /**
     * @param array $server
     * @param array $request
     * @param array $files
     */
    public function __construct($server = null, $request = null, $files = null)
    {
        $this->server = $server === null ? $_SERVER : $server;
        $this->request = $request === null ? $_REQUEST : $request;
        $this->files = new FileCollection($files);
    }

    /**
     * @return string
     */
    public function getMethod()
    {
        if (isset($this->server['HTTP_X_HTTP_METHOD_OVERRIDE'])) {
            return strtoupper($this->server['HTTP_X_HTTP_METHOD_OVERRIDE']);
        }

        if (isset($this->server['REQUEST_METHOD'])) {
            return strtoupper($this->server['REQUEST_METHOD']);
        }

        return 'GET';
    }

    /**
     * @return bool
     */
    public function isAjax()
    {
        if (isset($this->server['HTTP_X_REQUESTED_WITH'])) {
            return $this->server['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
        }
    }

    /**
     * @return bool
     */
    public function isGet()
    {
        return $this->getMethod() === 'GET';
    }

    /**
     * @return bool
     */
    public function isPost()
    {
        return $this->getMethod() === 'POST';
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return $this->getMethod() === 'DELETE';
    }

    /**
     * @return bool
     */
    public function isPut()
    {
        return $this->getMethod() === 'PUT';
    }

    /**
     * @param string $name
     * @param string $defaultValue
     * @return array|string
     */
    public function getRequest($name = null, $defaultValue = null)
    {
        if ($name === null) {
            return $this->request;
        }

        if (isset($this->request[$name])) {
            return $this->request[$name];
        }

        return $defaultValue;
    }

    /**
     * @param string $name
     * @param string $defaultValue
     * @return array|string
     */
    public function get($name = null, $defaultValue = null)
    {
        if (!$this->isGet()) {
            return null;
        }

        return $this->getRequest($name, $defaultValue);
    }

    /**
     * @param string $name
     * @param string $defaultValue
     * @return array|string
     */
    public function getPost($name = null, $defaultValue = null)
    {
        if (!$this->isPost()) {
            return null;
        }

        return $this->getRequest($name, $defaultValue);
    }

    /**
     * @param string $name
     * @param string $defaultValue
     * @return array|string
     */
    public function getPut($name = null, $defaultValue = null)
    {
        if (!$this->isPut()) {
            return null;
        }

        return $this->getRequest($name, $defaultValue);
    }

    /**
     * @param string $name
     * @param string $defaultValue
     * @return string|array
     */
    public function getDelete($name = null, $defaultValue = null)
    {
        if (!$this->isDelete()) {
            return null;
        }

        return $this->getRequest($name, $defaultValue);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasRequest($name)
    {
        return isset($this->request[$name]);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasGet($name)
    {
        if (!$this->isGet()) {
            return false;
        }

        return $this->hasRequest($name);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasPost($name)
    {
        if (!$this->isPost()) {
            return false;
        }

        return $this->hasRequest($name);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasPut($name)
    {
        if (!$this->isPut()) {
            return false;
        }

        return $this->hasRequest($name);
    }

    /**
     * @param string $name
     * @return bool
     */
    public function hasDelete($name)
    {
        if (!$this->isDelete()) {
            return false;
        }

        return $this->hasRequest($name);
    }

    /**
     * @return bool
     */
    public function hasFiles()
    {
        return (bool)$this->files->count();
    }

    /**
     * @return FileCollection
     */
    public function getFiles()
    {
        return $this->files;
    }

    /**
     * @return Headers
     */
    public function getHeaders()
    {
        if ($this->headers !== null) {
            return $this->headers;
        }

        $names = [
            'HTTP_HOST' => 'Host',
            'HTTP_CONNECTION' => 'Connection',
            'CONTENT_LENGTH' => 'Content-Length',
            'HTTP_CACHE_CONTROL' => 'Cache-Control',
            'HTTP_ACCEPT' => 'Accept',
            'HTTP_ORIGIN' => 'Origin',
            'HTTP_UPGRADE_INSECURE_REQUESTS' => 'Upgrade-Insecure-Requests',
            'HTTP_USER_AGENT' => 'User-Agent',
            'CONTENT_TYPE' => 'Content-Type',
            'HTTP_REFERER' => 'Referer',
            'HTTP_ACCEPT_ENCODING' => 'Accept-Encoding',
            'HTTP_ACCEPT_LANGUAGE' => 'Accept-Language'
        ];

        $this->headers = new Headers();

        foreach ($names as $key => $value) {
            if (isset($this->server[$key])) {
                $this->headers->set($value, $this->server[$key]);
            }
        }

        return $this->headers;
    }

    /**
     * @return string
     */
    public function getRequestUrl()
    {
        return $this->getServerEntry('REQUEST_URI');
    }

    /**
     * @return string
     */
    public function getScheme()
    {
        return $this->getServerEntry('REQUEST_SCHEME');
    }

    /**
     * @return string
     */
    public function getQueryString()
    {
        return $this->getServerEntry('QUERY_STRING');
    }

    /**
     * @return string
     */
    public function getScriptName()
    {
        return $this->getServerEntry('SCRIPT_NAME');
    }

    /**
     * @return string
     */
    public function getRemoteIp()
    {
        return $this->getServerEntry('REMOTE_ADDR');
    }

    private function getServerEntry($name)
    {
        if (isset($this->server[$name])) {
            return $this->server[$name];
        }
        return null;
    }
}