<?php

namespace Flares\Http;

use Flares\DI\ContainerInterface;
use Flares\DI\InjectionAwareInterface;
use Flares\Http\Cookie\CookieCollection;
use Flares\Storage\StorageInterface;

/**
 * Class Response
 * @package Flares\Http
 */
class Response implements ResponseInterface, InjectionAwareInterface
{
    /**
     * @var Headers
     */
    protected $headers;

    /**
     * @var CookieCollection
     */
    protected $cookies;

    /**
     * @var int
     */
    protected $statusCode = 200;

    /**
     * @var string
     */
    protected $content;

    /**
     * @var ContainerInterface
     */
    protected $di;

    /**
     * @var bool
     */
    protected $sent = false;

    protected static $statuses = [
        100 => 'Continue',
        101 => 'Switching Protocols',
        102 => 'Processing',
        118 => 'Connection timed out',

        200 => 'OK',
        201 => 'Created',
        202 => 'Accepted',
        203 => 'Non-Authoritative',
        204 => 'No Content',
        205 => 'Reset Content',
        206 => 'Partial Content',
        207 => 'Multi-Status',
        208 => 'Already Reported',
        210 => 'Content Different',
        226 => 'IM Used',

        300 => 'Multiple Choices',
        301 => 'Moved Permanently',
        302 => 'Found',
        303 => 'See Other',
        304 => 'Not Modified',
        305 => 'Use Proxy',
        306 => 'Reserved',
        307 => 'Temporary Redirect',
        308 => 'Permanent Redirect',
        310 => 'Too many Redirect',

        400 => 'Bad Request',
        401 => 'Unauthorized',
        402 => 'Payment Required',
        403 => 'Forbidden',
        404 => 'Not Found',
        405 => 'Method Not Allowed',
        406 => 'Not Acceptable',
        407 => 'Proxy Authentication Required',
        408 => 'Request Time-out',
        409 => 'Conflict',
        410 => 'Gone',
        411 => 'Length Required',
        412 => 'Precondition Failed',
        413 => 'Request Entity Too Large',
        414 => 'Request-URI Too Long',
        415 => 'Unsupported Media Type',
        416 => 'Requested range unsatisfiable',
        417 => 'Expectation failed',
        418 => 'I\'m a teapot',
        422 => 'Unprocessable entity',
        423 => 'Locked',
        424 => 'Method failure',
        425 => 'Unordered Collection',
        426 => 'Upgrade Required',
        428 => 'Precondition Required',
        429 => 'Too Many Requests',
        431 => 'Request Header Fields Too Large',
        449 => 'Retry With',
        450 => 'Blocked by Windows Parental Controls',

        500 => 'Internal Server Error',
        501 => 'Not Implemented',
        502 => 'Bad Gateway or Proxy Error',
        503 => 'Service Unavailable',
        504 => 'Gateway Time-out',
        505 => 'HTTP Version not supported',
        507 => 'Insufficient storage',
        508 => 'Loop Detected',
        509 => 'Bandwidth Limit Exceeded',
        510 => 'Not Extended',
        511 => 'Network Authentication Required',
    ];

    public function __construct()
    {
        $this->headers = new Headers();
        $this->cookies = new CookieCollection();
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @param string $content
     */
    public function appendContent($content)
    {
        $this->content .= $content;
    }

    /**
     * @param int $code
     * @throws Exception
     */
    public function setStatusCode($code)
    {
        if (!isset(self::$statuses[$code])) {
            throw new Exception('Incorrect status code');
        }

        $this->statusCode = $code;
    }

    /**
     * @return int
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param int $code
     * @return string
     * @throws Exception
     */
    public function getStatusMessage($code)
    {
        if (!isset(self::$statuses[$code])) {
            throw new Exception('Incorrect status code');
        }

        return self::$statuses[$code];
    }

    /**
     * @return Headers
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @param string $type
     * @param string $charset
     */
    public function setContentType($type, $charset = null)
    {
        if ($charset === null) {
            $this->headers->set('Content-Type', $type);
        } else {
            $this->headers->set('Content-Type', "; charset={$type}");
        }
    }

    /**
     * @param Headers $headers
     */
    public function setHeaders(Headers $headers)
    {
        $this->headers = $headers;
    }

    /**
     * @param string $url
     * @param int $statusCode
     * @throws Exception
     */
    public function redirect($url, $statusCode = 302)
    {
        $this->setStatusCode($statusCode);

        /**
         * @var $request Request
         */
        $request = $this->di->make('request');

        $redirectName = 'Location';

        if ($request->isAjax()) {
            $redirectName = 'X-Redirect';
        }

        $this->getHeaders()->set($redirectName, $url);
    }

    public function send()
    {
        if ($this->isSent()) {
            throw new Exception('Response was already sent');
        }

        $this->sendHttpStatus();
        $this->sendHeaders();
        $this->sendCookies();
        $this->sendContent();

        $this->sent = true;
    }

    /**
     * @param ContainerInterface $di
     */
    public function setDI(ContainerInterface $di)
    {
        $this->di = $di;
    }

    /**
     * @return ContainerInterface
     */
    public function getDI()
    {
        return $this->di;
    }

    /**
     * @return bool
     */
    public function isSent()
    {
        return $this->sent;
    }

    public  function clear()
    {
        $this->headers = new Headers();
        $this->cookies = new CookieCollection();
        $this->content = null;
        $this->statusCode = 200;
    }

    /**
     * @param CookieCollection $cookies
     */
    public function setCookies(CookieCollection $cookies)
    {
        $this->cookies = $cookies;
    }

    /**
     * @return CookieCollection
     */
    public function getCookies()
    {
        return $this->cookies;
    }

    /**
     * @param StorageInterface $storage
     * @param string $path
     * @param string $attachmentName
     * @param bool $attachment
     * @throws Exception
     */
    public function sendFile(StorageInterface $storage, $path, $attachmentName = null, $attachment = true)
    {
        if ($this->isSent()) {
            throw new Exception('Response was already sent');
        }

        $file = null;

        try {
            $file = $storage->getFile($path);
        } catch (\Flares\Storage\Exception $error) {
            throw new Exception('File not found');
        }

        if ($attachmentName === null) {
            $attachmentName = basename($path);
        }

        $this->sendHttpStatus();

        if ($attachment) {
            header('Content-Description: File Transfer');
			header('Content-Type: application/octet-stream');
			header('Content-Disposition: attachment; filename=' . $attachmentName);
			header('Content-Transfer-Encoding: binary');
        }

        echo $file;

        $this->sent = true;
    }

    protected function sendHeaders()
    {
        foreach ($this->headers as $header) {
            if ($header->getValue() === null) {
                header($header->getName() . ':' . $header->getValue(), true);
            } else {
                header($header->getName(), true);
            }
        }
    }

    protected function sendCookies()
    {
        foreach ($this->cookies as $cookie) {
            setcookie(
                $cookie->getName(),
                $cookie->getValue(),
                $cookie->getExpire(),
                $cookie->getPath(),
                $cookie->getDomain(),
                $cookie->getSecure(),
                $cookie->getHttpOnly()
            );
        }
    }

    protected function sendContent()
    {
        echo $this->content;
    }

    protected function sendHttpStatus()
    {
        header('HTTP/1.1 ' . $this->statusCode . ' ' . $this->getStatusMessage($this->statusCode));
    }
}