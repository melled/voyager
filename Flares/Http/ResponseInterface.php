<?php

namespace Flares\Http;

use Flares\Storage\StorageInterface;
use Flares\Http\Cookie\CookieCollection;

/**
 * Interface ResponseInterface
 * @package Flares\Http
 */
interface ResponseInterface
{
    /**
     * @param string $content
     */
    public function setContent($content);

    /**
     * @param string $content
     */
    public function appendContent($content);

    /**
     * @param int $code
     * @throws Exception
     */
    public function setStatusCode($code);

    /**
     * @return int
     */
    public function getStatusCode();

    /**
     * @param int $code
     * @return string
     * @throws Exception
     */
    public function getStatusMessage($code);

    /**
     * @return Headers
     */
    public function getHeaders();

    /**
     * @param string $type
     * @param string $charset
     */
    public function setContentType($type, $charset = null);

    /**
     * @param Headers $headers
     */
    public function setHeaders(Headers $headers);

    /**
     * @param string $url
     * @param int $statusCode
     * @throws Exception
     */
    public function redirect($url, $statusCode = 302);

    public function send();

    /**
     * @return bool
     */
    public function isSent();

    public  function clear();

    /**
     * @param CookieCollection $cookies
     */
    public function setCookies(CookieCollection $cookies);

    /**
     * @return CookieCollection
     */
    public function getCookies();

    /**
     * @param StorageInterface $storage
     * @param string $path
     * @param string $attachmentName
     * @param bool $attachment
     * @throws Exception
     */
    public function sendFile(StorageInterface $storage, $path, $attachmentName = null, $attachment = true);
}