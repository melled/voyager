<?php

namespace Flares\Autoload;

/**
 * Class ClassLoader
 * @package Flares\Autoload
 */
class ClassLoader
{
    protected $namespaces = [];

    protected $dirs = [];

    protected $classes = [];

    protected $appRoot;

    /**
     * @param string $appRoot
     */
    public function __construct($appRoot = '/')
    {
        $this->appRoot = $this->dirFix($appRoot);
    }

    /**
     * @param string $namespace
     * @param string $directory
     */
    public function registerNamespace($namespace, $directory)
    {
        $explodedNamespace = explode('\\', trim($namespace, '\\'));
        $directory = $this->dirFix($directory);
        $this->namespaces[] = [$explodedNamespace, $directory];
    }

    /**
     * @param string $directory
     */
    public function registerDir($directory)
    {
        $directory = $this->dirFix($directory);
        $this->dirs[] = $directory;
    }

    /**
     * @param string $className
     * @param string $file
     */
    public function registerClass($className, $file)
    {
        $className = trim($className, '\\');
        $this->classes[$className] = $file;
    }

    /**
     * @param string $class
     * @return bool
     */
    public function loadClass($class)
    {
        if ($this->loadFromClasses($class)) {
            return true;
        }

        if ($this->loadFromNamespaces($class)) {
            return true;
        }

        if ($this->loadFromDirs($class)) {
            return true;
        }

        if ($this->simpleLoad($class)) {
            return true;
        }

        return false;
    }

    /**
     * @param bool $prepend
     */
    public function register($prepend = false)
    {
        spl_autoload_register(array($this, 'loadClass'), true, $prepend);
    }

    public function unRegister()
    {
        spl_autoload_unregister(array($this, 'loadClass'));
    }

    protected function loadFile($file)
    {
        if (file_exists($file)) {
            require $file;
            return true;
        }
        return false;
    }

    protected function loadFromClasses($class)
    {
        foreach ($this->classes as $className => $file) {
            if ($class === $className && $this->loadFile($file)) {
                return true;
            }
        }
        return false;
    }

    protected function loadFromDirs($class)
    {
        $explodedClass = explode('\\', $class);

        foreach ($this->dirs as $dir) {
            $file = $this->getPathToFile($dir . DIRECTORY_SEPARATOR, $explodedClass);
            if ($this->loadFile($file)) {
                return true;
            }
        }
    }

    protected function loadFromNamespaces($class)
    {
        $explodedClass = explode('\\', $class);

        foreach ($this->namespaces as $namespace) {
            if (array_slice($explodedClass, 0, count($namespace[0])) === $namespace[0]) {
                $file = $this->getPathToFile($namespace[1], $explodedClass);
                if ($this->loadFile($file)) {
                    return true;
                }
            }
        }
    }

    protected function simpleLoad($class)
    {
        $explodedClass = explode('\\', $class);
        $file = $this->getPathToFile($this->appRoot, $explodedClass);
        if ($this->loadFile($file)) {
            return true;
        }
    }

    private function dirFix($directory)
    {
        return
            DIRECTORY_SEPARATOR .
            trim($directory, DIRECTORY_SEPARATOR) .
            DIRECTORY_SEPARATOR;
    }

    private function getPathToFile($dir, array $explodedClass)
    {
        return $dir . implode(DIRECTORY_SEPARATOR, $explodedClass) . '.php';
    }

}