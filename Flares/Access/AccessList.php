<?php

namespace Flares\Access;

/**
 * Class AccessList
 * @package Flares\Access
 */
class AccessList implements AccessListInterface
{
    protected $access = [];

    /**
     * @param string|int $level
     * @param string $resource
     * @param array|string $operations
     */
    public function allow($level, $resource, $operations)
    {
        $this->setAccess($level, $resource, $operations, true);
    }

    /**
     * @param string|int $level
     * @param string $resource
     * @param array|string $operations
     */
    public function deny($level, $resource, $operations)
    {
        $this->setAccess($level, $resource, $operations, false);
    }

    /**
     * @param string|int $level
     * @param string $resource
     * @param string $operation
     * @return bool
     */
    public function isAllowed($level, $resource, $operation)
    {
        $key = "{$level}:{$resource}:{$operation}";

        if (isset($this->access[$key])) {
            return $this->access[$key];
        }

        $anyOperation = "{$level}:{$resource}:*";

        if (isset($this->access[$anyOperation])) {
            return $this->access[$anyOperation];
        }

        return false;
    }

    protected function setAccess($level, $resource, $operations, $access)
    {
        if (is_array($operations)) {
            foreach ($operations as $operation) {
                $key = "{$level}:{$resource}:{$operation}";
                $this->access[$key] = $access;
            }
        } else {
            $key = "{$level}:{$resource}:{$operations}";
            $this->access[$key] = $access;
        }
    }
}