<?php

namespace Flares\Access;

/**
 * Interface AccessListInterface
 * @package Flares\Access
 */
interface AccessListInterface
{
    /**
     * @param string|int $level
     * @param string $resource
     * @param array|string $operations
     */
    public function allow($level, $resource, $operations);

    /**
     * @param string|int $level
     * @param string $resource
     * @param array|string $operations
     */
    public function deny($level, $resource, $operations);

    /**
     * @param string|int $level
     * @param string $resource
     * @param string $operation
     * @return bool
     */
    public function isAllowed($level, $resource, $operation);
}