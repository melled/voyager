<?php

namespace Flares\Database;

/**
 * Interface SelectStatement
 * @package Flares\Database
 */
interface SelectStatementInterface
{
    /**
     * @return bool
     */
    public function isDistinct();

    /**
     * @return array
     */
    public function getColumns();

    /**
     * @return string
     */
    public function getWhere();

    /**
     * @return array
     */
    public function getGroupBy();

    /**
     * @return string
     */
    public function getHaving();

    /**
     * @return array
     */
    public function getOrderBy();

    /**
     * @return array
     */
    public function getLimit();

    /**
     * @return array
     */
    public function getJoin();

    /**
     * @return bool
     */
    public function isForUpdate();

    /**
     * @return bool
     */
    public function isSharedLock();
}