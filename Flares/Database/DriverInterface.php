<?php

namespace Flares\Database;

interface DriverInterface extends ConnectionInterface {

    public function __construct(array $params, DialectInterface $dialect);

    public function connect(array $params);

    public function closeConnection();

    public function getDialect();

    public function getConnection();

    public function beginTransaction();

    public function commit();

    public function rollback();

    /*
    public function getListTables();

    public function getListView();

    public function tableExist($tableName);

    public function viewExist($viewName);

    public function dropTable($tableName, $ifExists = false);

    public function dropView($viewName, $ifExists = false);

    public function createTable(Table $table);

    public function createView($viewName, SelectStatementInterface $selectStatement);

    public function addColumn($tableName, Column $column);

    public function dropColumn($tableName, $columnName);

    public function modifyColumn($tableName, Column $column);

    public function addPrimaryKey();

    public function dropPrimaryKey();

    public function addForeignKey();

    public function dropForeignKey();

    public function addIndex();

    public function dropIndex();

    public function describeColumns();

    public function showColumn();

    public function showIndexes();

    public function select();

    public function update();

    public function insert();

    public function insertBatch();

    public function delete();
    */
}