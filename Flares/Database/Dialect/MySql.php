<?php

namespace Flares\Database\Dialect;

use Flares\Database\DialectInterface;
use Flares\Database\Schema\Column;
use Flares\Database\Schema\Table;
use Flares\Database\Schema\Type;
use Flares\Database\SelectStatementInterface;

/**
 * Class MySqlDialect
 * @package Flares\Database\Dialect
 */
class MySql implements DialectInterface
{
    public $types = [
        Type::BOOLEAN => 'BOOL',
        Type::FLOAT => 'FLOAT',
        Type::DOUBLE => 'DOUBLE',
        Type::DECIMAL => 'DECIMAL',
        Type::SMALLINT => 'SMALLINT',
        Type::INTEGER => 'INT',
        Type::BIGINT => 'BIGINT',
        Type::DATE => 'DATE',
        Type::TIME => 'TIME',
        Type::DATETIME => 'DATETIME',
        Type::STRING => 'VARCHAR',
        Type::TEXT => 'TEXT',
        Type::BLOB => 'BLOB'
    ];

    private $typeMap = [

    ];

    /**
     * @param string $tableName
     * @return string
     */
    public function getTableExistSql($tableName)
    {
        return "SELECT count(*)
                FROM `INFORMATION_SCHEMA`.`TABLES`
                WHERE `TABLE_NAME` = \"$tableName\"
                and `TABLE_SCHEMA` = DATABASE()";
    }

    /**
     * @param string $tableName
     * @param bool $ifExists
     * @return string
     */
    public function getDropTableSql($tableName, $ifExists = false)
    {
        $query = 'DROP TABLE ';
        $query .= $ifExists ? ' IF EXISTS ' : '';
        $query .= $this->getQuotedName($tableName);
        return $query;
    }

    /**
     * @return string
     */
    public function getShowTablesSql()
    {
        return 'SHOW TABLES';
    }

    /**
     * @param $viewName
     * @param bool $ifExists
     * @return string
     */
    public function getDropViewSql($viewName, $ifExists = false)
    {
        $query = 'DROP VIEW ';
        $query .= $ifExists ? ' IF EXISTS ' : '';
        $query .= $this->getQuotedName($viewName);
        return $query;
    }

    /**
     * @param string $tableName
     * @return string
     */
    public function getDropPrimaryKeySql($tableName)
    {
        return "ALTER TABLE `$tableName` DROP PRIMARY KEY";
    }

    /**
     * @param string $tableName
     * @param string $foreignKeyName
     * @return string
     */
    public function getDropForeignKeySql($tableName, $foreignKeyName)
    {
        return "ALTER TABLE `$tableName` DROP FOREIGN KEY `$foreignKeyName`";
    }

    /**
     * @param string $tableName
     * @param string $columnName
     * @return string
     */
    public function getDropColumnSql($tableName, $columnName)
    {
        return "ALTER TABLE `$tableName` DROP `$columnName`";
    }

    /**
     * @param string $tableName
     * @param string $indexName
     * @return string
     */
    public function getDropIndexSql($tableName, $indexName)
    {
        return "ALTER TABLE `$tableName` DROP KEY `$indexName`";
    }

    /**
     * @param string $viewName
     * @return string
     */
    public function getViewExistSql($viewName)
    {
        return "SELECT count(*)
                FROM `INFORMATION_SCHEMA`.`VIEWS`
                WHERE `TABLE_NAME` = \"$viewName\"
                and `TABLE_SCHEMA` = DATABASE()";
    }

    /**
     * @return string
     */
    public function getListViewsSql()
    {
        return "select `TABLE_NAME` as `VIEW_NAME`
                from `INFORMATION_SCHEMA`.`VIEWS`
                WHERE `TABLE_SCHEMA` = DATABASE()
                ORDER BY `VIEW_NAME`";
    }

    /**
     * @param string $tableName
     * @return string
     */
    public function getShowIndexesSql($tableName)
    {
        return "SHOW INDEXES FROM `$tableName`";
    }

    /**
     * @param string $tableName
     * @return string
     */
    public function getDescribeColumnsSql($tableName)
    {
        return "DESCRIBE `$tableName`";
    }

    /**
     * @param string $tableName
     * @param string $field
     * @return string
     */
    public function getShowColumnSql($tableName, $field)
    {
        return "SHOW COLUMNS
                FROM `$tableName`
                WHERE `Field` = \"$field\"";
    }

    /**
     * @param string $query
     * @param array $limit
     * @return string
     * @throws \Exception
     */
    public function getLimitSql($query, array $limit)
    {
        if ($limit && count($limit) <= 2) {
            return $query . ' LIMIT ' . implode(', ', $limit);
        }
        throw new \Exception('Bad limit');
    }

    /**
     * @param string $tableName
     * @param string $whereCondition
     * @return string
     */
    public function getDeleteSql($tableName, $whereCondition)
    {
        return "DELETE FROM `$tableName` WHERE $whereCondition";
    }

    /**
     * @param string $viewName
     * @param SelectStatementInterface $selectStatement
     * @return string
     */
    public function getCreateViewSql($viewName, SelectStatementInterface $selectStatement)
    {
        $sqlSelect = $this->getSelectSql($selectStatement);
        return "CREATE VIEW `$viewName` AS $sqlSelect";
    }

    /**
     * @param string $tableName
     * @param string $indexName
     * @param array $columnNames
     * @return string
     */
    public function getAddIndexSql($tableName, $indexName, array $columnNames)
    {
        $query = "CREATE INDEX `$indexName` ON `$tableName`";
        $query .= ' (' . implode(', ', $columnNames) . ')';
        return $query;
    }


    /**
     * @param string $tableName
     * @param array $columnNames
     * @return string
     */
    public function getAddPrimaryKeySql($tableName, array $columnNames)
    {
        $query = "ALTER TABLE `$tableName` ADD PRIMARY KEY";
        $query .= ' (' . implode(', ', $columnNames) . ')';
        return $query;
    }

    /**
     * @param string $tableName
     * @param string $columnNames
     * @param string $referenceTableName
     * @param string $referenceColumnNames
     * @return string
     */
    public function getAddForeignKeySql($tableName, $columnNames, $referenceTableName, $referenceColumnNames)
    {
        $query = "ALTER TABLE `$tableName` ADD FOREIGN KEY (";
        $query .= implode(', ', $columnNames) . ') REFERENCES ';
        $query .= "`$referenceTableName` (";
        $query .= implode(', ', $referenceColumnNames) . ")";
        return $query;
    }


    /**
     * @param string $tableName
     * @param array $values
     * @return string
     */
    public function getInsertSql($tableName, $values)
    {
        $query = "INSERT INTO `$tableName` ";
        $query .= '(`' . implode('`, `', array_keys($values)) .'`) ';
        $query .= "VALUES (" . implode(', ', $values) . ")";
        return $query;
    }

    /**
     * @param string $tableName
     * @param array $columns
     * @param array $values
     * @return string
     */
    public function getInsertBatchSql($tableName, array $columns, array $values)
    {
        $query = "INSERT INTO `$tableName` ";
        $query .= '(`' . implode('`, `', $columns) . '`) VALUES ';
        foreach ($values as $valuesPart) {
            var_dump($valuesPart);
            $query .= '(' . implode(', ', $valuesPart) . ') ';
        }
        return $query;
    }

    /**
     * @param string $tableName
     * @param array $set
     * @param string $whereDefinition
     * @return string
     */
    public function getUpdateSql($tableName, array $set, $whereDefinition = null)
    {
        $query = "UPDATE `$tableName` SET ";
        $setParts = [];
        foreach ($set as $column => $value) {
            $setParts[] = "`$column`=$value";
        }
        $query .= implode(', ', $setParts);
        if(!is_null($whereDefinition)) {
            $query .= " WHERE $whereDefinition";
        }
        return $query;
    }

    /**
     * @param Table $table
     * @return string
     */
    public function getCreateTableSql(Table $table)
    {
        $query = 'CREATE TABLE ' . $this->getQuotedName($table->getName());
        $columns = $table->getColumns();
        $columnsDefinition = [];
        foreach ($columns as $column) {
            $columnsDefinition[] = $this->getColumnDefinition($column);
        }
        $query .= ' (' . implode(', ', $columnsDefinition) . ')';
        return $query;
    }

    /**
     * @param string $tableName
     * @param Column $column
     * @return string
     */
    public function getAddColumnSql($tableName, Column $column)
    {
        $query = "ALTER TABLE `$tableName` ADD ";
        $query .= $this->getColumnDefinition($column);
        $query .= ' ' . $this->getColumnPosition($column);
        return $query;
    }

    /**
     * @param string $tableName
     * @param Column $column
     * @return string
     */
    public function getModifyColumnSql($tableName, Column $column)
    {
        $query = "ALTER TABLE `$tableName` MODIFY ";
        $query .= $this->getColumnDefinition($column);
        $query .= ' ' . $this->getColumnPosition($column);
        return $query;
    }

    /**
     * @param SelectStatementInterface $definition
     * @return string
     */
    public function getSelectSql(SelectStatementInterface $definition)
    {
        $query = 'SELECT ';
        $query .= $this->getDistinctPart($definition);
        $query .= $this->getColumnsPart($definition);
        $query .= $this->getFromPart($definition);
        $query .= $this->getJoinPart($definition);
        $query .= $this->getWherePart($definition);
        $query .= $this->getGroupByPart($definition);
        $query .= $this->getHavingPart($definition);
        $query .= $this->getOrderByPart($definition);
        $query .= $this->getLimitPart($definition);
        $query .= $this->getLockingReadsPart($definition);
        return $query;
    }

    private function getDistinctPart(SelectStatementInterface $statement)
    {
        return $statement->isDistinct() ? 'DISTINCT' : '';
    }

    private function getColumnsPart(SelectStatementInterface $statement)
    {
        return ' ' . implode(', ', $statement->getColumns());
    }

    private function getFromPart(SelectStatementInterface $statement)
    {
        return ' FROM `' . implode('`, `', $statement->getFrom()) . '`';
    }

    private function getJoinPart(SelectStatementInterface $statement)
    {
        $joins = $statement->getJoin();
        $compiledJoins = [];
        foreach ($joins as $join) {
            $compiledJoin = $join['type'] ? ' ' . $join['type'] : ' ';
            $compiledJoin .= ' JOIN `' . $join['table'] . '`';
            if ($join['condition']) {
                $compiledJoin .= ' ON ' . $join['condition'];
            }
            $compiledJoins[] = $compiledJoin;
        }
        return implode(' ', $compiledJoins);
    }

    private function getWherePart(SelectStatementInterface $statement)
    {
        $where = $statement->getWhere();
        return $where ? ' WHERE ' . $statement->getWhere() : '';
    }

    private function getGroupByPart(SelectStatementInterface $statement)
    {
        $groupBy = $statement->getGroupBy();
        $query = '';
        if ($groupBy['by']) {
            $query = ' GROUP BY ' . implode(', ', $groupBy['by']);
            if ($groupBy['direction']) {
                $query .= ' ' . $groupBy['direction'];
            }
        }
        return $query;
    }

    private function getHavingPart(SelectStatementInterface $statement)
    {
        $having = $statement->getHaving();
        return $having ? ' HAVING ' . $having : '';
    }

    private function getOrderByPart(SelectStatementInterface $statement)
    {
        $orderBy = $statement->getOrderBy();
        $query = '';
        if ($orderBy['by']) {
            $query = ' ORDER BY ' . implode(', ', $orderBy['by']);
            if ($orderBy['direction']) {
                $query .= ' ' . $orderBy['direction'];
            }
        }
        return $query;
    }

    private function getLimitPart(SelectStatementInterface $statement)
    {
        $limit = $statement->getLimit();
        if ($limit[0] != 0 || $limit[1] != 0) {
            return $this->getLimitSql('', $limit);
        }
        return '';
    }

    private function getLockingReadsPart(SelectStatementInterface $statement)
    {
        if ($statement->isForUpdate()) {
            return ' FOR UPDATE';
        } elseif ($statement->isSharedLock()) {
            return ' LOCK IN SHARE MODE';
        }
        return '';
    }

    private function getQuotedName($name)
    {
        return "`$name`";
    }

    private function getColumnPosition(Column $column)
    {
        $query = $column->isFirst() ? 'FIRST' : '';
        if ($column->getAfterPosition()) {
            $query = 'AFTER `' . $column->getAfterPosition() . '`';
        }
        return $query;
    }

    private function getUnsignedPart(Column $column)
    {
        return $column->isUnsigned() ? ' UNSIGNED' : '';
    }

    private function getNotNullPart(Column $column)
    {
        return $column->isNotNull() ? ' NOT NULL' : '';
    }

    private function getAutoIncrementPart(Column $column)
    {
        return $column->isAutoIncrement() ? ' AUTO_INCREMENT' : '';
    }

    private function getPrimaryKeyPart(Column $column)
    {
        return $column->isPrimaryKey() ? ' PRIMARY KEY' : '';
    }

    private function getDefaultvaluePart(Column $column)
    {
        if (!is_null($column->getDefaultValue())) {
            return " DEFAULT \"" . addcslashes($column->getDefaultValue(), "\"") . "\"";
        }
        return '';
    }

    private function getSizePart(Column $column)
    {
        if ($column->getSize()) {
            return '(' . $column->getSize() . ')';
        }
        return '';
    }

    private function getTypePart(Column $column)
    {
        return ' ' . $this->types[$column->getType()];
    }

    private function getColumnDefinition(Column $column)
    {
        $query = $this->getQuotedName($column->getName());
        $query .= $this->getTypePart($column);
        $query .= $this->getSizePart($column);
        $query .= $this->getUnsignedPart($column);
        $query .= $this->getNotNullPart($column);
        $query .= $this->getDefaultvaluePart($column);
        $query .= $this->getAutoIncrementPart($column);
        $query .= $this->getPrimaryKeyPart($column);
        return $query;
    }
}