<?php

namespace Flares\Database;

interface StatementInterface
{
    public function fetch($mode = null);

    public function fetchAll($mode = null);

    public function fetchColumn($index = 0);

    public function setFetchMode($mode);

    public function columnCount();

    public function closeCursor();

    public function bindValue($param, $value, $type = null);

    public function bindParam($column, &$var, $type = null, $len = null);

    public function errorCode();

    public function errorInfo();

    public function execute($params = null);

    public function rowCount();
}