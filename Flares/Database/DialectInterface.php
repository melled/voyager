<?php

namespace Flares\Database;

use Flares\Database\Schema\Column;
use Flares\Database\Schema\Table;

interface DialectInterface
{
    /**
     * @param string $tableName
     * @return string
     */
    //public function getTableExistSql($tableName);

    /**
     * @param string $tableName
     * @param bool $ifExists
     * @return string
     */
    //public function getDropTableSql($tableName, $ifExists = false);

    /**
     * @return string
     */
    //public function getShowTablesSql();

    /**
     * @param $viewName
     * @param bool $ifExists
     * @return string
     */
    //public function getDropViewSql($viewName, $ifExists = false);

    /**
     * @param string $tableName
     * @return string
     */
    //public function getDropPrimaryKeySql($tableName);

    /**
     * @param string $tableName
     * @param string $foreignKeyName
     * @return string
     */
    //public function getDropForeignKeySql($tableName, $foreignKeyName);

    /**
     * @param string $tableName
     * @param string $columnName
     * @return string
     */
    //public function getDropColumnSql($tableName, $columnName);

    /**
     * @param string $tableName
     * @param string $indexName
     * @return string
     */
    //public function getDropIndexSql($tableName, $indexName);

    /**
     * @param string $viewName
     * @return string
     */
    //public function getViewExistSql($viewName);

    /**
     * @return string
     */
    //public function getListViewsSql();

    /**
     * @param string $tableName
     * @return string
     */
    //public function getShowIndexesSql($tableName);

    /**
     * @param string $tableName
     * @return string
     */
    //public function getDescribeColumnsSql($tableName);

    /**
     * @param string $tableName
     * @param string $field
     * @return string
     */
    //public function getShowColumnSql($tableName, $field);

    /**
     * @param string $query
     * @param array $limit
     * @return string
     * @throws \Exception
     */
    //public function getLimitSql($query, array $limit);

    /**
     * @param string $tableName
     * @param string $whereCondition
     * @return string
     */
    //public function getDeleteSql($tableName, $whereCondition);

    /**
     * @param string $viewName
     * @param SelectStatementInterface $selectStatement
     * @return string
     */
    //public function getCreateViewSql($viewName, SelectStatementInterface $selectStatement);

    /**
     * @param string $tableName
     * @param string $indexName
     * @param array $columnNames
     * @return string
     */
    //public function getAddIndexSql($tableName, $indexName, array $columnNames);

    /**
     * @param string $tableName
     * @param array $columnNames
     * @return string
     */
    //public function getAddPrimaryKeySql($tableName, array $columnNames);

    /**
     * @param string $tableName
     * @param string $columnNames
     * @param string $referenceTableName
     * @param string $referenceColumnNames
     * @return string
     */
    //public function getAddForeignKeySql($tableName, $columnNames, $referenceTableName, $referenceColumnNames);


    /**
     * @param string $tableName
     * @param array $values
     * @return string
     */
    //public function getInsertSql($tableName, $values);

    /**
     * @param string $tableName
     * @param array $columns
     * @param array $values
     * @return string
     */
    //public function getInsertBatchSql($tableName, array $columns, array $values);

    /**
     * @param string $tableName
     * @param array $set
     * @param string $whereDefinition
     * @return string
     */
    //public function getUpdateSql($tableName, array $set, $whereDefinition = null);

    /**
     * @param Table $table
     * @return string
     */
    //public function getCreateTableSql(Table $table);

    /**
     * @param string $tableName
     * @param Column $column
     * @return string
     */
    //public function getAddColumnSql($tableName, Column $column);

    /**
     * @param string $tableName
     * @param Column $column
     * @return string
     */
    //public function getModifyColumnSql($tableName, Column $column);

    /**
     * @param SelectStatementInterface $definition
     * @return string
     */
    //public function getSelectSql(SelectStatementInterface $definition);
}