<?php

namespace Flares\Database;

use \PDO;

interface ConnectionInterface {

    /**
     * @param $string
     * @return StatementInterface
     */
    public function prepare($string);

    /**
     * @return StatementInterface
     */
    public function query();

    /**
     * @param string $string
     * @param int $type
     * @return string
     */
    public function quote($string, $type = PDO::PARAM_STR);

    /**
     * @param string $statement
     * @return int
     */
    public function exec($statement);

    /**
     * @param string $name
     * @return int
     */
    public function lastInsertId($name);

    /**
     * @return bool
     */
    public function beginTransaction();

    /**
     * @return bool
     */
    public function commit();

    /**
     * @return bool
     */
    public function rollback();

    /**
     * @return bool
     */
    public function inTransaction();

    /**
     * @return mixed
     */
    public function errorCode();

    /**
     * @return array
     */
    public function errorInfo();

}