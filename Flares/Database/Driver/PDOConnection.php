<?php

namespace Flares\Database\Driver;

use Flares\Database\ConnectionInterface;

use \PDO;

class PDOConnection extends PDO implements ConnectionInterface
{
    public function prepare($statement, $options = [])
    {
        $stmt = parent::prepare($statement, $options);
        return new PDOStatement($stmt);
    }

    public function query()
    {
        $args = func_get_args();
        $argsCount = func_num_args();

        $statement = null;

        switch ($argsCount) {
            case 1:
                $statement = parent::query($args[0]);
                break;
            case 2:
                $statement = parent::query($args[0], $args[1]);
                break;
            case 3:
                $statement = parent::query($args[0], $args[1], $args[2]);
                break;
            case 4:
                $statement = parent::query($args[0], $args[1], $args[2], $args[3]);
                break;
        }

        return new PDOStatement($statement);
    }
}