<?php

namespace Flares\Database\Driver;

use Flares\Database\StatementInterface;

class PDOStatement implements StatementInterface
{
    /**
     * @var \PDOStatement
     */
    private $statement;


    public function __construct(\PDOStatement $statement)
    {
        $this->statement = $statement;
    }

    public function fetch($mode = null)
    {
        return $this->statement->fetch($mode);
    }

    public function fetchAll($mode = null)
    {
        return $this->statement->fetchAll($mode);
    }

    public function fetchColumn($index = 0)
    {
        return $this->statement->fetchColumn($index);
    }

    public function setFetchMode($mode)
    {
        return $this->statement->setFetchMode($mode);
    }

    public function columnCount()
    {
        return $this->statement->columnCount();
    }

    public function closeCursor()
    {
        $this->statement->closeCursor();
    }

    public function bindValue($param, $value, $type = PDO::PARAM_STR)
    {
        return $this->statement->bindValue($param, $value, $type);
    }

    public function bindParam($column, &$var, $type = \PDO::PARAM_STR, $len = null)
    {
        return $this->statement->bindParam($column, $var, $type, $len);
    }

    public function errorCode()
    {
        return $this->statement->errorCode();
    }

    public function errorInfo()
    {
        return $this->statement->errorInfo();
    }

    public function execute($params = null)
    {
        return $this->statement->execute($params);
    }

    public function rowCount()
    {
        return $this->statement->rowCount();
    }
}