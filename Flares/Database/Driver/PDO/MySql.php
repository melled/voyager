<?php

namespace Flares\Database\Driver\PDO;

use Flares\Database\Driver\AbstractPDODriver;
use Flares\Database\Schema\Column;
use Flares\Database\Schema\Table;
use Flares\Database\SelectStatementInterface;

class MySql extends AbstractPDODriver
{
    protected $type = 'mysql';

    public function getListTables()
    {
        $sql = $this->dialect->getShowTablesSql();
        $result = $this->query($sql)->fetchAll(\PDO::FETCH_COLUMN);
        return $result;
    }

    public function createTable(Table $table)
    {
        $sql = $this->dialect->getCreateTableSql($table);
        $result = $this->exec($sql);
        return $result;
    }

    public function tableExist($tableName)
    {
        $sql = $this->dialect->getTableExistSql($tableName);
        $result = $this->query($sql)->fetchColumn();
        return (bool)$result;
    }

    public function viewExist($viewName)
    {
        $sql = $this->dialect->getViewExistSql($viewName);
        $result = $this->query($sql)->fetchColumn();
        return (bool)$result;
    }

    public function dropTable($tableName)
    {
        $sql = $this->dialect->getDropTableSql($tableName);
        $result = $this->exec($sql);
        return $result === false ? false : true;
    }

    public function dropView($viewName)
    {
        $sql = $this->dialect->getDropViewSql($viewName);
        $result = $this->exec($sql);
        return $result === false ? false : true;
    }

    public function dropPrimaryKey($tableName)
    {
        $sql = $this->dialect->getDropPrimaryKeySql($tableName);
        $result = $this->exec($sql);
        return $result === false ? false : true;
    }

    public function dropForeignKey($tableName, $keyName)
    {
        $sql = $this->dialect->getDropForeignKeySql($tableName, $keyName);
        $result = $this->exec($sql);
        return $result === false ? false : true;
    }

    public function dropColumn($tableName, $columnName)
    {
        $sql = $this->dialect->getDropColumnSql($tableName, $columnName);
        $result = $this->exec($sql);
        return $result === false ? false : true;
    }

    public function dropIndex($tableName, $indexName)
    {
        $sql = $this->dialect->getDropIndexSql($tableName, $indexName);
        $result = $this->exec($sql);
        return $result === false ? false : true;
    }

    public function getListViews()
    {
        $sql = $this->dialect->getListViewsSql();
        $result = $this->query($sql)->fetchAll(\PDO::FETCH_COLUMN);
        return $result;
    }

    public function showIndexes($tableName)
    {
        $sql = $this->dialect->getShowIndexesSql($tableName);
        $result = $this->query($sql)->fetchAll(\PDO::FETCH_NAMED);
        $indexes = [];
        foreach ($result as $index) {
            if (isset($indexes[$index['Key_name']])) {
                $indexes[$index['Key_name']][] = $index['Column_name'];
            } else {
                $indexes[$index['Key_name']] = [$index['Column_name']];
            }
        }
        return $indexes;
    }

    public function describeColumns($table)
    {

    }

    public function showColumn($tableName, $columnName)
    {
        $sql = $this->dialect->getShowColumnSql($tableName, $columnName);
        $result = $this->query($sql)->fetchAll(\PDO::FETCH_ASSOC)[0];
        $name = $result['Field'];
        $definition['type'] = l;
        $type =
        $definition = [];

        if ($result['Extra'] == 'auto_increment') {
            $definition['autoIncrement'] = true;
        }
        return $result;
    }

    public function delete($tableName, $where, $params = null)
    {
        $sql = $this->dialect->getDeleteSql($tableName, $where);
        $statement = $this->prepare($sql);
        return $statement->execute($params);
    }

    public function addPrimaryKey($tableName, array $columnNames)
    {
        $sql = $this->dialect->getAddPrimaryKeySql($tableName, $columnNames);
        $result = $this->exec($sql);
        return $result === false ? false : true;
    }

    public function addForeignKey($tableName, $columnNames, $referenceTableName, $referenceColumnNames)
    {
        $sql = $this->dialect->getAddForeignKeySql($tableName, $columnNames, $referenceTableName, $referenceColumnNames);
        $result = $this->exec($sql);
        return $result;
    }

    public function addIndex($tableName, $indexName, array $columnNames)
    {
        $sql = $this->dialect->getAddIndexSql($tableName, $indexName, $columnNames);
        $result = $this->exec($sql);
        return $result === false ? false : true;
    }

    public function insert($tableName, $values)
    {
        foreach ($values as &$value) {
            $value = $this->quote($value);
        }
        $sql = $this->dialect->getInsertSql($tableName, $values);
        var_dump($sql);
        $result = $this->exec($sql);
        return $result;
    }

    public function insertBatch($tableName, array $columns, array $values)
    {
        $markers = $values;
        foreach ($markers as &$marker) {
            $marker = array_fill(0, count($marker), '?');
        }
        vardump($markers);
        //$sql = $this->dialect->getInsertBatchSql($tableName, $columns, $markers);
        //var_dump($sql);
        //$result = $this->prepare($sql);
    }

    public function addColumn($tableName, Column $column)
    {
        $sql = $this->dialect->getAddColumnSql($tableName, $column);
        $result = $this->exec($sql);
        return $result === false ? false : true;
    }

    public function modifyColumn($tableName, Column $column)
    {
        $sql = $this->dialect->getModifyColumnSql($tableName, $column);
        $result = $this->exec($sql);
        return $result === false ? false : true;
    }

    public function select(SelectStatementInterface $select, $params = null)
    {
        $sql = $this->dialect->getSelectSql($select);
        vardump($sql);
        $statement = $this->prepare($sql);
        $result = $statement->execute($params);
        if ($result) {
            return $statement->fetchAll(\PDO::FETCH_ASSOC);
        }
        return false;
    }

    public function update($tableName, array $set, $whereDefinition = null, $params = null)
    {
        $sql = $this->dialect->getUpdateSql($tableName, $set, $whereDefinition);
        $statement = $this->prepare($sql);
        $result = $statement->execute($params);
        return $result;
    }

    public function createView($viewName, SelectStatementInterface $select, $params = null)
    {
        $sql = $this->dialect->getCreateViewSql($viewName, $select);
        $statement = $this->prepare($sql);
        $result = $statement->execute($params);
        return $result;
    }
}