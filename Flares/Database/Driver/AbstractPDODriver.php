<?php

namespace Flares\Database\Driver;

use Flares\Database\DialectInterface;
use Flares\Database\DriverInterface;
use PDO;

abstract class AbstractPDODriver implements DriverInterface
{
    /**
     * @var PDOConnection
     */
    protected $connection;

    protected $dialect;

    public function __construct(array $params, DialectInterface $dialect)
    {
        $this->connect($params);
        $this->dialect = $dialect;
    }

    public function connect(array $params)
    {
        $host = @$params['host'];
        $user = @$params['user'];
        $passwd = @$params['password'];
        $db = @$params['dbname'];
        $options = @$params['options'];
        $this->connection = new PDOConnection("$this->type:dbname=$db;host=$host", $user, $passwd, $options);
    }

    public function disconnect()
    {
        $this->connection = null;
    }

    public function inTransaction()
    {
        return $this->connection->inTransaction();
    }

    public function prepare($string)
    {
        return $this->connection->prepare($string);
    }

    public function query()
    {
        $args = func_get_args();
        $argsCount = func_num_args();
        vardump($args[0]);

        switch ($argsCount) {
            case 1:
                return $this->connection->query($args[0]);
            case 2:
                return $this->connection->query($args[0], $args[1]);
            case 3:
                return $this->connection->query($args[0], $args[1], $args[2]);
            case 4:
                return $this->connection->query($args[0], $args[1], $args[2], $args[3]);
            default:
                return null;
        }
    }

    public function quote($string, $type = PDO::PARAM_STR)
    {
        return $this->connection->quote($string, $type);
    }

    public function exec($statement)
    {
        vardump($statement);
        return $this->connection->exec($statement);
    }

    public function lastInsertId($name = null)
    {
        return $this->connection->lastInsertId($name);
    }

    public function beginTransaction()
    {
        return $this->connection->beginTransaction();
    }

    public function commit()
    {
        return $this->connection->commit();
    }

    public function rollback()
    {
        return $this->connection->rollBack();
    }

    public function errorCode()
    {
        return $this->connection->errorCode();
    }

    public function errorInfo()
    {
        return $this->connection->errorInfo();
    }

    public function getDialect()
    {
        return $this->dialect;
    }

    private function isPdoObject()
    {
        if ($this->connection instanceof PDO) {
            return true;
        }
        return false;
    }
}