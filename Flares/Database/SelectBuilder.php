<?php

namespace Flares\Database;

class SelectBuilder implements SelectStatementInterface
{
    private $parts = [
        'distinct' => false,
        'columns' => [],
        'from' => [],
        'join' => [],
        'where' => '',
        'groupBy' => [
            'direction' => null,
            'by' => []
        ],
        'having' => '',
        'orderBy' => [
            'direction' => null,
            'by' => []
        ],
        'limit' => [0, 0],
        'forUpdate' => false,
        'sharedLock' => false
    ];

    /**
     * @return $this
     */
    public function distinct()
    {
        $this->parts['distinct'] = true;
        return $this;
    }

    /**
     * @param array|string $columns
     * @return $this
     */
    public function columns($columns)
    {
        if (is_array($columns)) {
            $this->parts['columns'] = $columns;
        } else {
            $this->parts['columns'] = [$columns];
        }
        return $this;
    }

    /**
     * @param array|string $from
     * @return $this
     */
    public function from($from)
    {
        if (is_array($from)) {
            $this->parts['from'] = $from;
        } else {
            $this->parts['from'] = [$from];
        }
        return $this;
    }

    /**
     * @param string $type
     * @param string $table
     * @param string $condition
     * @return $this
     */
    public function join($type, $table, $condition = null)
    {
        $this->parts['join'][] = [
            'type' => $type,
            'condition' => $condition,
            'table' => $table
        ];
        return $this;
    }

    /**
     * @param string $where
     * @return $this
     */
    public function where($where)
    {
        if ($this->parts['where']) {
            $this->parts['where'] .= ' AND ' . $where;
        } else {
            $this->parts['where'] = $where;
        }
        return $this;
    }

    /**
     * @param string $where
     * @return $this
     */
    public function orWhere($where)
    {
        if ($this->parts['where']) {
            $this->parts['where'] .= ' OR ' . $where;
        } else {
            $this->parts['where'] = $where;
        }
        return $this;
    }

    /**
     * @param array|string $groupBy
     * @param string $direction
     * @return $this
     */
    public function groupBy($groupBy, $direction = '')
    {
        if (is_array($groupBy)) {
            $this->parts['groupBy']['by'] = $groupBy;
        } else {
            $this->parts['groupBy']['by'] = [$groupBy];
        }
        $this->parts['groupBy']['direction'] = $direction;
        return $this;
    }

    /**
     * @param string $having
     * @return $this
     */
    public function having($having)
    {
        if ($this->parts['having']) {
            $this->parts['having'] .= ' AND ' . $having;
        } else {
            $this->parts['having'] = $having;
        }
        return $this;
    }

    /**
     * @param string $having
     * @return $this
     */
    public function orHaving($having)
    {
        if ($this->parts['having']) {
            $this->parts['having'] .= ' OR ' . $having;
        } else {
            $this->parts['having'] = $having;
        }
        return $this;
    }

    /**
     * @param array|string $orderBy
     * @param string $direction
     * @return $this
     */
    public function orderBy($orderBy, $direction = '')
    {
        if (is_array($orderBy)) {
            $this->parts['orderBy']['by'] = $orderBy;
        } else {
            $this->parts['orderBy']['by'] = [$orderBy];
        }
        $this->parts['orderBy']['direction'] = $direction;
        return $this;
    }

    /**
     * @param int $limit
     * @param int $offset
     * @return $this
     */
    public function limit($limit, $offset = 0)
    {
        $this->parts['limit'] = [$limit, $offset];
        return $this;
    }

    public function isDistinct()
    {
        return $this->parts['distinct'];
    }

    public function getColumns()
    {
        return $this->parts['columns'];
    }

    public function getWhere()
    {
        return $this->parts['where'];
    }

    public function getFrom()
    {
        return $this->parts['from'];
    }

    public function getGroupBy()
    {
        return $this->parts['groupBy'];
    }

    public function getHaving()
    {
        return $this->parts['having'];
    }

    public function getOrderBy()
    {
        return $this->parts['orderBy'];
    }

    public function getLimit()
    {
        return $this->parts['limit'];
    }

    public function isForUpdate()
    {
        return $this->parts['forUpdate'];
    }

    public function isSharedLock()
    {
        return $this->parts['sharedLock'];
    }

    public function getJoin()
    {
        return $this->parts['join'];
    }


}