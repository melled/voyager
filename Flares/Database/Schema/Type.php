<?php

namespace Flares\Database\Schema;

class Type
{
    const BOOLEAN = 'boolean';

    const FLOAT = 'float';

    const DOUBLE = 'double';

    const DECIMAL = 'decimal';

    const SMALLINT = 'smallint';

    const INTEGER = 'integer';

    const BIGINT = 'bigint';

    const DATE = 'date';

    const TIME = 'time';

    const DATETIME = 'datetime';

    const STRING = 'string';

    const TEXT = 'text';

    const BLOB = 'blob';

    const ENUM = 'enum';
}