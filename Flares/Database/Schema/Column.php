<?php

namespace Flares\Database\Schema;

/**
 * Class Column
 * @package Flares\Database\Schema
 */
class Column
{
    private $name;

    private $autoIncrement = false;

    private $notNull = false;

    private $primaryKey = false;

    private $defaultValue = null;

    private $size = 0;

    private $unsigned = false;

    private $type = null;

    private $number = false;

    private $first = false;

    private $after = null;

    /**
     * @param string $name
     * @param array $definition
     * @throws \Exception
     */
    public function __construct($name, array $definition = [])
    {
        $this->name = $name;

        if (isset($definition['type'])) {
            $this->type = $definition['type'];
        } else {
            throw new \Exception();
        }

        if (isset($definition['autoIncrement'])) {
            $this->autoIncrement = $definition['autoIncrement'];
        }

        if (isset($definition['notNull'])) {
            $this->notNull = $definition['notNull'];
        }

        if (isset($definition['primaryKey'])) {
            $this->primaryKey = $definition['primaryKey'];
        }

        if (isset($definition['defaultValue'])) {
            $this->defaultValue = $definition['defaultValue'];
        }

        if (isset($definition['size'])) {
            $this->size = $definition['size'];
        }

        if (isset($definition['unsigned'])) {
            $this->unsigned = $definition['unsigned'];
        }

        if (isset($definition['isNumber'])) {
            $this->number = $definition['isNumber'];
        }

        if (isset($definition['first'])) {
            $this->first = $definition['first'];
        }

        if(isset($definition['afterPosition'])) {
            $this->after = $definition['afterPosition'];
        }
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function isAutoIncrement()
    {
        return $this->autoIncrement;
    }

    /**
     * @return bool
     */
    public function isNumber()
    {
        return $this->number;
    }

    /**
     * @return mixed
     */
    public function isNotNull()
    {
        return $this->notNull;
    }

    /**
     * @return bool
     */
    public function isPrimaryKey()
    {
        return $this->primaryKey;
    }

    /**
     * @return bool
     */
    public function isUnsigned()
    {
        return $this->unsigned;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return mixed
     */
    public function getDefaultValue()
    {
        return $this->defaultValue;
    }

    /**
     * @return int
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @return bool
     */
    public function isFirst()
    {
        return $this->first;
    }

    public function getAfterPosition()
    {
        return $this->after;
    }
}