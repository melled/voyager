<?php

namespace Flares\Database\Schema;

class Table
{
    private $name;

    private $columns = [];

    public function __construct($name, array $columns = [])
    {
        $this->name = $name;
        $this->columns = $columns;
    }

    public function addColumn($column)
    {
        $this->columns[] = $column;
    }

    public function removeColumn($name)
    {
        foreach ($this->columns as &$column) {
            if ($column->getName() === $name) {
                unset($column);
            }
        }
    }

    public function getColumns()
    {
        return $this->columns;
    }

    public function getColumn($name)
    {
        foreach ($this->columns as $column) {
            if ($column === 'name') {
                return $column;
            }
        }
    }

    public function hasColumn($name)
    {
        foreach ($this->columns as $column) {
            if ($column === 'name') {
                return true;
            }
        }
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }
}