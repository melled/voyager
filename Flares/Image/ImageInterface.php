<?php

namespace Flares\Image;

use Flares\Storage\StorageInterface;

/**
 * Interface ImageInterface
 * @package Flares\Image
 */
interface ImageInterface
{
    /**
     * @return int
     */
    public function getWidth();

    /**
     * @return int
     */
    public function getHeight();

    /**
     * @return string
     */
    public function getImageType();

    /**
     * @return string
     */
    public function getMimeType();

    /**
     * @return StorageInterface
     */
    public function getStorage();

    /**
     * @return string
     */
    public function getFile();

    /**
     * @param int $width
     * @param int $height
     * @param bool $keepRatio
     * @throws Exception
     */
    public function resize($width = null, $height = null, $keepRatio = false);

    /**
     * @param StorageInterface $storage
     * @param string $file
     * @param int $quality
     */
    public function save(StorageInterface $storage = null, $file = null, $quality = 100);

    /**
     * @param string $extension
     * @param int $quality
     * @return string
     */
    public function render($extension = 'png', $quality = 100);

    /**
     * @param int $radius
     */
    public function blur($radius);

    /**
     * @param int $degrees
     * @param string $background
     */
    public function rotate($degrees, $background = 'black');

    public function flip();

    public function flop();

    /**
     * @param int $width
     * @param int $height
     * @param int $x
     * @param int $y
     */
    public function crop($width, $height, $x = 0, $y = 0);

    /**
     * @param int $width
     * @param int $height
     */
    public function fit($width, $height);

    /**
     * @param int $width
     * @param int $height
     */
    public function fitAndCrop($width, $height);

    /**
     * @param ImageInterface $image
     * @param int $offsetX
     * @param int $offsetY
     * @param double $opacity
     */
    public function drawWatermark(ImageInterface $image, $offsetX = 0, $offsetY = 0, $opacity = 1.0);

    /**
     * @param string $text
     * @param int $gravity
     * @param int $offsetX
     * @param int $offsetY
     * @param string $color
     * @param double $opacity
     * @param int $size
     * @param string $fontFile
     */
    public function drawText($text, $gravity = self::GRAVITY_CENTER, $offsetX = 0, $offsetY = 0, $color = '#000000', $opacity = 1.0, $size = 12, $fontFile = null);
}