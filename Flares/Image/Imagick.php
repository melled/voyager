<?php

namespace Flares\Image;

use \Flares\Storage\StorageInterface;

/**
 * Class Imagick
 * @package Flares\Image
 */
class Imagick
{
    const GRAVITY_SOUTHEAST = \Imagick::GRAVITY_SOUTHEAST;

    const GRAVITY_SOUTHWEST = \Imagick::GRAVITY_SOUTHWEST;

    const GRAVITY_SOUTH = \Imagick::GRAVITY_SOUTH;

    const GRAVITY_NORTHEAST = \Imagick::GRAVITY_NORTHEAST;

    const GRAVITY_NORTHWEST = \Imagick::GRAVITY_NORTHWEST;

    const GRAVITY_NORTH = \Imagick::GRAVITY_NORTH;

    const GRAVITY_CENTER = \Imagick::GRAVITY_CENTER;

    const GRAVITY_EAST = \Imagick::GRAVITY_EAST;

    const GRAVITY_WEST = \Imagick::GRAVITY_WEST;

    /**
     * @var int
     */
    protected $width;

    /**
     * @var int
     */
    protected $height;

    /**
     * @var StorageInterface
     */
    protected $storage;

    /**
     * @var string
     */
    protected $file;

    /**
     * @var \Imagick
     */
    protected $image;

    /**
     * @var int
     */
    protected $imageType;

    /**
     * @var string
     */
    protected $mimeType;

    /**
     * @param StorageInterface $storage
     * @param string $file
     * @param int $width
     * @param int $height
     */
    public function __construct(StorageInterface $storage, $file, $width = null, $height = null)
    {
        $this->storage = $storage;
        $this->file = $file;

        $image = $this->image = new \Imagick();

        if ($storage->exists($file)) {
            $image->readImageBlob($storage->getFile($file));

            if (!$image->getImageAlphaChannel()) {
                $image->setImageAlphaChannel(\Imagick::ALPHACHANNEL_SET);
            }
        } else {
            $image->newImage($width, $height, new \ImagickPixel('transparent'));
            $image->setFormat('png');
            $image->setImageFormat('png');
        }

        $this->width = $image->getImageWidth();
        $this->height = $image->getImageHeight();

        $this->imageType = $image->getImageType();
        $this->mimeType = $image->getImageMimeType();
    }

    /**
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * @return string
     */
    public function getImageType()
    {
        return $this->imageType;
    }

    /**
     * @return string
     */
    public function getMimeType()
    {
        return $this->mimeType;
    }

    /**
     * @return StorageInterface
     */
    public function getStorage()
    {
        return $this->storage;
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * @param int $width
     * @param int $height
     * @param bool $keepRatio
     * @throws Exception
     */
    public function resize($width = null, $height = null, $keepRatio = false)
    {
        if (!$keepRatio) {
            if ($width === null) {
                $width = $this->width;
            }

            if ($height === null) {
                $height = $this->height;
            }
        } else {
            if ($width === null) {
                $width = (int)($this->width * $height / $this->height);
            } else if ($height === null) {
                $height = (int)($this->height * $width / $this->width);
            } else {
                throw new Exception('One of the dimensions must not be given');
            }
        }

        $this->image->scaleImage($width, $height);

        $this->width = $width;
        $this->height = $height;
    }

    /**
     * @param StorageInterface $storage
     * @param string $file
     * @param int $quality
     */
    public function save(StorageInterface $storage = null, $file = null, $quality = 100)
    {
        if ($storage === null) {
            $storage = $this->storage;
        }

        if ($file === null) {
            $file = $this->file;
        }

        $extension = strtolower($storage->getFileExtension($file));

        $storage->putFile($file, $this->render($extension, $quality));
    }

    /**
     * @param string $extension
     * @param int $quality
     * @return string
     */
    public function render($extension = 'png', $quality = 100)
    {
        $extension = strtolower($extension);

        $image = $this->image;

        $image->setFormat($extension);
        $image->setImageFormat($extension);

        $this->imageType = $image->getImageType();
        $this->mimeType = $image->getImageMimeType();

        if ($extension === 'gif') {
            $image->optimizeImageLayers();
        } elseif ($extension === 'jpg' || $extension === 'jpeg') {
            $image->setImageCompression(\Imagick::COMPRESSION_JPEG);
        }

        $image->setImageCompressionQuality($quality);

        return $image->getImageBlob();
    }

    /**
     * @param int $radius
     */
    public function blur($radius)
    {
        if ($radius > 100) {
            $radius = 100;
        }

        if ($radius < 0) {
            $radius = 0;
        }

        $this->image->blurImage($radius, 100);
    }

    /**
     * @param int $degrees
     * @param string $background
     */
    public function rotate($degrees, $background = 'black')
    {
        $bg = new \ImagickPixel($background);
        $this->image->rotateImage($bg, $degrees);
    }

    public function flip()
    {
        $this->image->flipImage();
    }

    public function flop()
    {
        $this->image->flopImage();
    }

    /**
     * @param int $width
     * @param int $height
     * @param int $x
     * @param int $y
     */
    public function crop($width, $height, $x = 0, $y = 0)
    {
        $this->image->cropImage($width, $height, $x, $y);
    }

    /**
     * @param int $width
     * @param int $height
     */
    public function fit($width, $height)
    {
        $this->image->scaleImage($width, $height, true);
    }

    /**
     * @param int $width
     * @param int $height
     */
    public function fitAndCrop($width, $height)
    {
        $this->image->cropThumbnailImage($width, $height);
    }

    /**
     * @param ImageInterface $image
     * @param int $offsetX
     * @param int $offsetY
     * @param double $opacity
     */
    public function drawWatermark(ImageInterface $image, $offsetX = 0, $offsetY = 0, $opacity = 1.0)
    {
        $waterMark = new \Imagick();
        $waterMark->readImageBlob($image->render());
        $waterMark->setImageOpacity($opacity);
        $this->image->compositeImage($waterMark, \Imagick::COMPOSITE_OVER, $offsetX, $offsetY);
    }

    /**
     * @param string $text
     * @param int $gravity
     * @param int $offsetX
     * @param int $offsetY
     * @param string $color
     * @param double $opacity
     * @param int $size
     * @param string $fontFile
     */
    public function drawText($text, $gravity = self::GRAVITY_CENTER, $offsetX = 0, $offsetY = 0, $color = '#000000', $opacity = 1.0, $size = 12, $fontFile = null)
    {
        $draw = new \ImagickDraw();
        $textColor = new \ImagickPixel($color);
        $draw->setFillColor($textColor);
        $draw->setFillOpacity($opacity);
        $draw->setGravity($gravity);
        $draw->setFontSize($size);

        if ($fontFile) {
            $draw->setFont($fontFile);
        }

        $this->image->annotateImage($draw, $offsetX, $offsetY, 0, $text);

    }
}