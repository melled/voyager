<?php

namespace Flares\Events;

interface EventsManagerInterface
{
    /**
     * @param string $eventType
     * @param $listener
     * @param int $priority
     * @return mixed
     */
    public function attach($eventType, $listener, $priority = 0);

    /**
     * @param string $eventType
     * @param $listener
     * @return mixed
     */
    public function detach($eventType, $listener);

    /**
     * @param string $eventType
     * @return void
     */
    public function detachAll($eventType = null);

    /**
     * @param string $eventType
     * @param EventsProviderInterface $provider
     * @param mixed $data
     * @return mixed
     */
    public function fire($eventType, $provider, $data = null);

    /**
     * @param string $eventType
     * @return array
     */
    public function getListeners($eventType);
}