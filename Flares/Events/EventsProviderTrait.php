<?php

namespace Flares\Events;

/**
 * Class Provider
 * @package Flares\Events
 */
trait EventsProviderTrait
{
    private $eventsManager;

    /**
     * @param EventsManagerInterface $eventsManager
     */
    public function setEventsManager($eventsManager)
    {
        $this->eventsManager = $eventsManager;
    }

    /**
     * @return EventsManagerInterface
     */
    public function getEventsManager()
    {
        return $this->eventsManager;
    }
}