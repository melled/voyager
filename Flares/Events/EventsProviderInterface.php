<?php

namespace Flares\Events;

/**
 * Interface EventsProviderInterface
 * @package Flares\Events
 */
interface EventsProviderInterface
{
    /**
     * @param EventsManagerInterface $eventsManager
     */
    public function setEventsManager($eventsManager);

    /**
     * @return EventsManagerInterface
     */
    public function getEventsManager();
}