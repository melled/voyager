<?php

namespace Flares\Events;

use \Closure;
use \SplPriorityQueue;

/**
 * Class Manager
 * @package Flares\Events
 */
class EventsManager implements EventsManagerInterface
{
    /**
     * @var SplPriorityQueue[]
     */
    private $events = [];

    public function attach($eventType, $listener, $priority = 0)
    {
        if (@$this->events[$eventType] instanceof SplPriorityQueue) {
            $this->events[$eventType]->insert($listener, $priority);
        } else {
            $this->events[$eventType] = new SplPriorityQueue();
            $this->events[$eventType]->insert($listener, $priority);
        }
    }

    public function detach($eventType, $listener)
    {
        if (@$this->events[$eventType] === null) {
            return;
        }

        $handlers = $this->events[$eventType];

        $newHandlers = new SplPriorityQueue();

        $handlers->setExtractFlags(SplPriorityQueue::EXTR_BOTH);
        $handlers->top();

        foreach ($handlers as $handler) {
            if ($handler['data'] !== $listener) {
                $newHandlers->insert(
                    $handler['data'],
                    $handler['priority']
                );
            }
        }

        $this->events[$eventType] = $newHandlers;
    }

    /**
     * @param string $eventType
     * @param EventsProviderInterface $provider
     * @param array $data
     * @return array
     * @throws Exception
     */
    public function fire($eventType, $provider, $data = [])
    {
        if (!strpos($eventType, ':')) {
            throw new Exception('Invalid event type');
        }

        $type = explode(':', $eventType)[0];
        $name = explode(':', $eventType)[1];

        if (!array_key_exists($type, $this->events)) {
            return [];
        }

        $this->events[$type]->setExtractFlags(SplPriorityQueue::EXTR_DATA);
        if ($this->events[$type]->isEmpty()) {
            return [];
        }

        $this->events[$type]->top();

        $parameters = [$name, $provider, $data];

        foreach ($this->events[$type] as $listener) {
            if ($listener instanceof Closure) {
                call_user_func_array($listener, $parameters);
            } elseif (is_object($listener)) {
                if (method_exists($listener, $name)) {
                    $listener->{$name}($name, $provider, $data);
                } else {
                    throw new Exception("Method '$name' not found in class " . get_class($listener));
                }
            }
        }
    }

    /**
     * @param string $eventType
     * @return void
     */
    public function detachAll($eventType = null)
    {
        if ($eventType === null) {
            $this->events = [];
        } else {
            $this->events[$eventType] = null;
        }
    }

    /**
     * @param string $eventType
     * @return array
     */
    public function getListeners($eventType)
    {
        if (!isset($this->events[$eventType])) {
            return [];
        }

        $listeners = [];

        foreach ($this->events[$eventType] as $listener) {
            $listeners[] = $listener;
        }

        return $listeners;
    }
}