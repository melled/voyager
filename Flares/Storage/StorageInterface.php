<?php

namespace Flares\Storage;

/**
 * Interface StorageInterface
 * @package Flares\Storage
 */
interface StorageInterface
{
    /**
     * @param string $path
     * @return bool
     */
    public function deleteFile($path);

    /**
     * @param string $path
     * @return bool
     */
    public function exists($path);

    /**
     * @param string $path
     * @param string $to
     * @return bool
     */
    public function move($path, $to);

    /**
     * @param string $path
     * @param string $to
     * @return bool
     */
    public function copy($path, $to);

    /**
     * @param string $path
     * @return int
     */
    public function getFileSize($path);

    /**
     * @param string $path
     * @return bool
     */
    public function isDir($path);

    /**
     * @param string $path
     * @return bool
     */
    public function isFile($path);

    /**
     * @param string $path
     * @return bool
     */
    public function isWritable($path);

    /**
     * @param string $path
     * @param int $chmod
     * @param bool $recursive
     * @return bool
     */
    public function makeDir($path, $chmod = 0777, $recursive = false);

    /**
     * @param string $path
     * @param bool $includeDirectories
     * @return string[]
     */
    public function getListFiles($path, $includeDirectories = false);

    /**
     * @param string $path
     * @return string
     */
    public function getMimeType($path);

    /**
     * @param string $path
     * @return string
     */
    public function getFileType($path);

    /**
     * @param string $path
     * @return string
     */
    public function getFileExtension($path);

    /**
     * @param string $path
     * @return string
     */
    public function getFileName($path);

    /**
     * @param string $path
     * @return string
     */
    public function getFile($path);

    /**
     * @param string $path
     * @param mixed $data
     * @return int
     */
    public function putFile($path, $data);

    /**
     * @param string $path
     * @param mixed $data
     * @return int
     */
    public function appendToFile($path, $data);

    /**
     * @param string $path
     * @param mixed $data
     * @return int
     */
    public function prependToFile($path, $data);

    /**
     * @param string $path
     * @return bool
     */
    public function deleteDir($path);

    /**
     * @param string $from
     * @param string $to
     * @return bool
     */
    public function copyDir($from, $to);

    /**
     * @param string $path
     * @return int
     */
    public function getLastModified($path);
}