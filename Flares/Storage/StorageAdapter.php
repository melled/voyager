<?php

namespace Flares\Storage;

class StorageAdapter implements StorageAdapterInterface
{
    protected $driver;

    public function __construct(StorageInterface $driver)
    {
        $this->driver = $driver;
    }

    public function getDriver()
    {
        return $this->driver;
    }

    public function deleteFile($path)
    {
        return $this->driver->deleteFile($path);
    }

    public function exists($path)
    {
        return $this->driver->exists($path);
    }

    public function move($path, $to)
    {
        return $this->driver->move($path, $to);
    }

    public function copy($path, $to)
    {
        return $this->driver->copy($path, $to);
    }

    public function getFileSize($path)
    {
        return $this->driver->getFileSize($path);
    }

    public function isDir($path)
    {
        return $this->driver->isDir($path);
    }

    public function isFile($path)
    {
        return $this->driver->isFile($path);
    }

    public function isWritable($path)
    {
        return $this->driver->isWritable($path);
    }

    public function makeDir($path, $chmod = 0777, $recursive = false)
    {
        return $this->driver->makeDir($path, $chmod, $recursive);
    }

    public function deleteDir($path)
    {
        return $this->driver->deleteDir($path);
    }

    public function copyDir($from, $to)
    {
        return $this->driver->copyDir($from, $to);
    }

    public function getListFiles($path, $includeDirectories = false)
    {
        return $this->driver->getListFiles($path, $includeDirectories);
    }

    public function getMimeType($path)
    {
        return $this->driver->getMimeType($path);
    }

    public function getLastModified($path)
    {
        return $this->driver->getLastModified($path);
    }

    public function getFileType($path)
    {
        return $this->driver->getFileType($path);
    }

    public function getFileExtension($path)
    {
        return $this->driver->getFileExtension($path);
    }

    public function getFileName($path)
    {
        return $this->driver->getFileName($path);
    }

    public function getFile($path)
    {
        return $this->driver->getFile($path);
    }

    public function putFile($path, $data)
    {
        return $this->driver->putFile($path, $data);
    }

    public function appendToFile($path, $data)
    {
        return $this->driver->appendToFile($path, $data);
    }

    public function prependToFile($path, $data)
    {
        return $this->driver->prependToFile($path, $data);
    }
}