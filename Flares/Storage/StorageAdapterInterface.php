<?php

namespace Flares\Storage;

interface StorageAdapterInterface extends StorageInterface
{
    /**
     * @param StorageInterface $driver
     */
    public function __construct(StorageInterface $driver);

    /**
     * @return StorageInterface
     */
    public function getDriver();
}