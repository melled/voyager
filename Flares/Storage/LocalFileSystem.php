<?php

namespace Flares\Storage;

/**
 * Class LocalFileSystem
 * @package Flares\Storage
 */
class LocalFileSystem implements StorageInterface
{
    /**
     * @param string $path
     * @return bool
     */
    public function deleteFile($path)
    {
        try {
            return (bool) @unlink($path);
        } catch (Exception $error) {
            return false;
        }
    }

    /**
     * @param string $path
     * @return bool
     */
    public function exists($path)
    {
        return file_exists($path);
    }

    /**
     * @param string $path
     * @param string $to
     * @return bool
     */
    public function move($path, $to)
    {
        try {
            return (bool) @rename($path, $to);
        } catch (Exception $error) {
            return false;
        }
    }

    /**
     * @param string $path
     * @param string $to
     * @return bool
     */
    public function copy($path, $to)
    {
        try {
            return (bool) @copy($path, $to);
        } catch (Exception $error) {
            return false;
        }
    }

    /**
     * @param string $path
     * @return int
     */
    public function getFileSize($path)
    {
        return filesize($path);
    }

    /**
     * @param string $path
     * @return bool
     */
    public function isDir($path)
    {
        return is_dir($path);
    }

    /**
     * @param string $path
     * @return bool
     */
    public function isFile($path)
    {
        return is_file($path);
    }

    /**
     * @param string $path
     * @return bool
     */
    public function isWritable($path)
    {
        return is_writable($path);
    }

    /**
     * @param string $path
     * @param int $chmod
     * @param bool $recursive
     * @return bool
     */
    public function makeDir($path, $chmod = 0777, $recursive = false)
    {
        return @mkdir($path, $chmod, $recursive);
    }

    /**
     * @param string $path
     * @return bool
     */
    public function deleteDir($path)
    {
        if (!$this->isDir($path)) {
            return false;
        }

        $files = new \FilesystemIterator($path);

        foreach ($files as $file) {
            if ($file->isDir()) {
                $this->deleteDir($file->getPathname());
            } else {
                $this->deleteFile($file->getPathname());
            }
        }

        return rmdir($path);
    }

    /**
     * @param string $from
     * @param string $to
     * @return bool
     */
    public function copyDir($from, $to)
    {
        if (!$this->isDir($from)) {
            return false;
        }

        if (!$this->isDir($to)) {
            $this->makeDir($to, 0777, true);
        }

        $files = new \FilesystemIterator($from);

        foreach ($files as $file) {
            $target = $to . '/' . $file->getBasename();

            if ($file->isDir()) {
                $path = $file->getPathname();

                if (!$this->copyDir($path, $target)) {
                    return false;
                }
            } else {
                if (!$this->copy($file->getPathname(), $target)) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @param string $path
     * @param bool $includeDirectories
     * @return array
     */
    public function getListFiles($path, $includeDirectories = false)
    {
        $files = glob($path . '/*');

        if ($includeDirectories) {
            return $files;
        }

        return array_filter($files, function($file) {
            return filetype($file) == 'file';
        });
    }

    /**
     * @param string $path
     * @return mixed
     */
    public function getMimeType($path)
    {
        $finfo = finfo_open(FILEINFO_MIME_TYPE);
        return finfo_file($finfo, $path);
    }

    /**
     * @param string $path
     * @return int
     */
    public function getLastModified($path)
    {
        return filemtime($path);
    }

    /**
     * @param string $path
     * @return string
     */
    public function getFileType($path)
    {
        return filetype($path);
    }

    /**
     * @param string $path
     * @return mixed
     */
    public function getFileExtension($path)
    {
        return pathinfo($path, PATHINFO_EXTENSION);
    }

    /**
     * @param string $path
     * @return mixed
     */
    public function getFileName($path)
    {
        return pathinfo($path, PATHINFO_FILENAME);
    }

    /**
     * @param string $path
     * @return string
     * @throws Exception
     */
    public function getFile($path)
    {
        if ($this->isFile($path)) {
            return file_get_contents($path);
        }

        throw new Exception('File does not exist');
    }

    /**
     * @param string $path
     * @param mixed $data
     * @return int
     */
    public function putFile($path, $data)
    {
        return file_put_contents($path, $data);
    }

    /**
     * @param string $path
     * @param mixed $data
     * @return int
     */
    public function appendToFile($path, $data)
    {
        return file_put_contents($path, $data, FILE_APPEND);
    }

    /**
     * @param string $path
     * @param mixed $data
     * @return int
     * @throws Exception
     */
    public function prependToFile($path, $data)
    {
        return $this->putFile($path, $data . $this->getFile($path));
    }

    /**
     * @param string $path
     * @param int $line
     * @return string mixed
     * @throws Exception
     */
    public function getLine($path, $line)
    {
        $lines = @file($path);

        if (!$lines) {
            throw new Exception('File not found');
        }

        if (!isset($lines[$line])) {
            throw new Exception('Line not exists');
        }

        return $lines[$line];
    }
}