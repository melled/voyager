<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

function vardump($var) {
    echo '<pre style="background: #99FF99; padding: 5px; border-radius: 5px; border: 1px solid #66FF66;">';
    var_dump($var);
    echo '</pre>';
}

function printr($var) {
    echo '<pre style="background: #99FF99; padding: 5px; border-radius: 5px; border: 1px solid #66FF66;">';
    print_r($var);
    echo '</pre>';
}
