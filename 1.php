<form method="get">
    <h4>Размерность массива</h4>
	n = <input type="text" name="n"><br><br>
    m = <input type="text" name="m"><br><br>
    <h4>Выбрать элемент массива</h4>
    x = <input type="text" name="x"><br><br>
    y = <input type="text" name="y"><br><br>
	<input type="submit">
</form>

<?php

$m = $_GET['m'];
$n = $_GET['n'];

$x = $_GET['x'];
$y = $_GET['y'];

$result = [];

for ($i = 0; $i < $m; $i++) {
    for ($j = 0; $j < $n; $j++) {
        $result[$i][$j] = (($i % 2 == 0) && ($j % 2 == 0)) ? 1 : -1;
    }
}

echo '<table>';
for ($i = 0; $i < $m; $i++) {
    echo '<tr>';
    for ($j = 0; $j < $n; $j++) {
        echo '<td>' . $result[$i][$j] . '</td>';
    }
    echo '</tr>';
}
echo '</table>';

echo '<br><b>Выбранный элемент: </b>' . $result[$x][$y] . ', тип: ' . gettype($result[$x][$y]);

?>
